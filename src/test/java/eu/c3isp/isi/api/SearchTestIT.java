package eu.c3isp.isi.api;

import java.net.URI;
import java.util.Base64;
import java.util.LinkedList;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.cnr.iit.isi.api.restapi.types.xacml.RequestAttributes;
import it.cnr.iit.isi.api.restapi.types.xacml.RequestContainer;
import it.cnr.iit.isi.api.restapi.types.xacml.RequestElement;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)

@ActiveProfiles("test") // load application-test.properties
@AutoConfigureMockMvc
@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class SearchTestIT {

	String kentSearchString = "{\n" + "\"combiningRule\": \"or\",\n" + "\"criteria\": [\n" + "{\n"
			+ "\"attribute\": \"event_type\",\n" + "\"operator\": \"eq\",\n" + "\"value\": \"Firewall Event\"\n" + "}\n"
			+ "]\n" + "}";

	@Value("${isi.api.url}")
	String isiApiURL;

	@Test
	public void testSearch() throws JsonProcessingException {

		String searchApi = "/v1/search/dpos/true";

		ObjectMapper mapper = new ObjectMapper();

		String plainCreds = "user:password";
		byte[] plainCredsBytes = plainCreds.getBytes();
		byte[] base64CredsBytes = Base64.getEncoder().encode(plainCredsBytes);
		String base64Creds = new String(base64CredsBytes);

		MultiValueMap<String, Object> parts = new LinkedMultiValueMap<String, Object>();

		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Basic " + base64Creds);
//	headers.add("X-c3isp-input_metadata", new RequestContainer().toString());

		headers.setContentType(MediaType.APPLICATION_JSON);

		RequestContainer container = new RequestContainer();
		RequestElement el = new RequestElement();
		RequestAttributes attr = new RequestAttributes();
		attr.setAttributeId("ns:c3isp:search-string");

		attr.setValue(kentSearchString);
		LinkedList<RequestAttributes> list = new LinkedList<RequestAttributes>();
		list.add(attr);
		el.setAttributes(list);
		container.setRequest(el);
		System.err.println("in searchTestIT");
		System.err.println(mapper.writeValueAsString(container));
		System.err.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(container));

//	parts.add("input_metadata", container);
//	parts.add("params",container);

		/**
		 * PLESE NOTE: Test as this is a post, I put the value directly in
		 */
//	HttpEntity<MultiValueMap<String, Object>> request = 
//			new HttpEntity<MultiValueMap<String, Object>>(parts, headers);

		HttpEntity request = new HttpEntity(container, headers);
		System.err.println(headers);

		RestTemplate restTemplate = TestUtils.createSSLIgnoringRestTemplate();

		URI searchUri = UriComponentsBuilder.fromUriString(isiApiURL).path(searchApi).build().toUri();

		ResponseEntity<String> response = restTemplate.exchange(searchUri, HttpMethod.POST, request, String.class);

//	System.err.println("cco_id is: " +result.getCco_id());
		System.err.println("dpo_id is: " + response.getBody());

	}

}
