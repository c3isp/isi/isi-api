package eu.c3isp.isi.api;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.Base64;
import java.util.LinkedList;
import java.util.Map;

import javax.net.ssl.SSLContext;

import org.apache.commons.io.FileUtils;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.tomcat.util.digester.DocumentProperties.Charset;
//import org.apache.commons.collections.map.MultiValueMap;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
//import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
//import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.cnr.iit.isi.api.restapi.impl.ApiCreateDPO;
import it.cnr.iit.isi.api.restapi.types.RestResponse;
import it.cnr.iit.isi.api.restapi.types.xacml.RequestAttributes;
import it.cnr.iit.isi.api.restapi.types.xacml.RequestContainer;
import it.cnr.iit.isi.api.restapi.types.xacml.RequestElement;

@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)

@ActiveProfiles("test") // load application-test.properties
@AutoConfigureMockMvc
@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class CreateDPOTestLocalhost {
	
	String test = ApiCreateDPO.EXAMPLERESTMETADATA;
	String testMetadata = "{            \"id\" : \"4000123\",       \"dsa_id\" :  \"DSA-56976731-3c16-46cc-a4e1-8384c6208eb0\",       \"start_time\" : \"2017-12-14T12:00:00.0Z\",       \"end_time\" : \"2017-12-14T18:01:01.0Z\",       \"event_type\" : \"Firewall Event\",       \"organization\" : \"3DRepo\" }";
	
	String testOnlyDPOMetadataMirko = "{\"id\":\"4000123\",\"dsa_id\":\"DSA-56976731-3c16-46cc-a4e1-8384c6208eb0\",\"start_time\":\"2017-12-14T12:00:00.0Z\",\"end_time\":\"2017-12-14T18:01:01.0Z\",\"event_type\":\"Firewall Event\",\"organization\":\"3DRepo\"}";

	String testMetadataMirko = "{\"Request\":{\"Attribute\":[{\"AttributeId\":\"ns:c3isp:dpo-metadata\",\"Value\":\"{\\\"id\\\":\\\"4000123\\\",\\\"dsa_id\\\":\\\"DSA-56976731-3c16-46cc-a4e1-8384c6208eb0\\\",\\\"start_time\\\":\\\"2017-12-14T12:00:00.0Z\\\",\\\"end_time\\\":\\\"2017-12-14T18:01:01.0Z\\\",\\\"event_type\\\":\\\"Firewall Event\\\",\\\"organization\\\":\\\"3DRepo\\\"}\",\"DataType\":\"string\"}]}}";
	
	String testMetadataMirkoWrong = "{\"Request\":{\"Attribute\":[{\"AttributeId\":\"ns:c3isp:dpo-metadata\",\"Value\":{\"id\":\"4000123\",\"dsa_id\":\"DSA-56976731-3c16-46cc-a4e1-8384c6208eb0\",\"start_time\":\"2017-12-14T12:00:00.0Z\",\"end_time\":\"2017-12-14T18:01:01.0Z\",\"event_type\":\"Firewall Event\",\"organization\":\"3DRepo\"},\"DataType\":\"string\"}]}}";
	
	String dsaId = "DSA-56976731-3c16-46cc-a4e1-8384c6208eb0";
	
	
//    @LocalServerPort
    int randomServerPort;

	@Value("${isi.api.url}")
    String isiApiURL ;


    
	@Autowired
	TestRestTemplate restTemplate;
	
//	@Test	
	public void testCreate() throws IOException {

///////////////////////////////////////
// CREATE
///////////////////////////////////////

		
		String publishApi = "/v1/dpo";
		
		isiApiURL += randomServerPort;
		
		URL fileURL = this.getClass().getClassLoader().getResource("response.txt");

		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> jsonMap = mapper.readValue(testMetadata,
		    new TypeReference<Map<String,Object>>(){});
		
		String plainCreds = "user:password";
		byte[] plainCredsBytes = plainCreds.getBytes();
		byte[] base64CredsBytes = Base64.getEncoder().encode(plainCredsBytes);
		String base64Creds = new String(base64CredsBytes);

		RequestContainer container = new RequestContainer();
		RequestElement el = new RequestElement();
		
		RequestAttributes attr = new RequestAttributes();
		attr.setAttributeId("ns:c3isp:dpo-metadata");
		attr.setValue(mapper.writeValueAsString(jsonMap));
		
		RequestAttributes dsaIdAttr = new RequestAttributes();
		dsaIdAttr.setAttributeId("ns:c3isp:dsa-id");
		dsaIdAttr.setValue(dsaId);
		
		
		LinkedList<RequestAttributes> list = new LinkedList<RequestAttributes>();
		
		
		list.add(attr);
		list.add(dsaIdAttr);
		
		el.setAttributes(list);
		container.setRequest(el);
		
		System.err.println("create metadata");
		System.err.println(mapper.writeValueAsString(container));
		System.err.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(container));
		
		MultiValueMap<String, Object> parts = new LinkedMultiValueMap<String, Object>();

		parts.add("input_metadata", container);
		parts.add("fileToSubmit", new FileSystemResource(new File(fileURL.getFile())) );
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Basic " + base64Creds);

		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		HttpEntity<MultiValueMap<String, Object>> request = 
				new HttpEntity<MultiValueMap<String, Object>>(parts, headers);
		
		System.err.println(headers.getContentType());
		
//		TestRestTemplate restTemplate = new TestRestTemplate(httpClientOptions)
//		TestRestTemplate restTemplate = new TestRestTemplate(createSSLIgnoringRestTemplate());

		RestTemplate restTemplate = TestUtils.createSSLIgnoringRestTemplate();		
		
		ResponseEntity<RestResponse> response = restTemplate//.withBasicAuth("user", "password")
				.exchange(isiApiURL+publishApi, HttpMethod.POST, request, RestResponse.class);
		
		RestResponse respBody = response.getBody();
		
		String dposId = respBody.getAdditionalProperties().get("dposId");
		
		System.err.println("dpo_id is: " + dposId);
		
///////////////////////////////////////
		// READ
///////////////////////////////////////
		
		MultiValueMap<String, Object> parts1 = new LinkedMultiValueMap<String, Object>();

//		parts.add("input_metadata", container);
//		parts.add("fileToSubmit", new FileSystemResource(new File(fileURL.getFile())) );
		
	
		
		HttpHeaders headers1 = new HttpHeaders();
		headers1.add("Authorization", "Basic " + base64Creds);
//		headers1.add("X-c3isp-input_metadata", URLEncoder.encode("{}", "UTF-8"));
		headers1.add("X-c3isp-input_metadata", "{}");
			
//		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		HttpEntity<MultiValueMap<String, Object>> request1 = 
				new HttpEntity<MultiValueMap<String, Object>>(parts1, headers1);
		
		System.err.println(request1);
		
//		ResponseEntity<FileSystemResource> readResponse = restTemplate//.withBasicAuth("user", "password")
//				.exchange(
//						isiApiURL+publishApi+"/"+dposId+"/", 
//						HttpMethod.GET, 
//						request1, 
//						FileSystemResource.class);
		
		System.err.println("read metadata");
		
		
		ResponseEntity<ByteArrayResource> readResponse = restTemplate//.withBasicAuth("user", "password")
		.exchange(
				isiApiURL+publishApi+"/"+dposId+"/", 
				HttpMethod.GET, 
				request1, 
				ByteArrayResource.class);
		
		
		ByteArrayResource file = readResponse.getBody(); 
		File tmpFile = File.createTempFile("udd", ".fff");
		FileUtils.forceDeleteOnExit(tmpFile);
		FileUtils.copyInputStreamToFile(file.getInputStream(), tmpFile);
		assertTrue(FileUtils.contentEquals(tmpFile, new File(fileURL.getFile())));
		
//		ResponseEntity<String> deleteResponse = 
//				restTemplate.delete(
//				isiApiURL+publishApi+"/"+dposId+"/");
		
		
		
/// DELETE
		
		MultiValueMap<String, Object> partsDelete = new LinkedMultiValueMap<String, Object>();
		
	
		
		HttpHeaders headersDelete = new HttpHeaders();
		headersDelete.add("Authorization", "Basic " + base64Creds);
		headersDelete.add("X-c3isp-input_metadata", "{}");
			
		headersDelete.setContentType(MediaType.APPLICATION_JSON_UTF8);
		HttpEntity<MultiValueMap<String, Object>> requestDelete = 
				new HttpEntity<MultiValueMap<String, Object>>(partsDelete, headersDelete);

		
		
		ResponseEntity<String> deleteResponse = restTemplate//.withBasicAuth("user", "password")
				.exchange(
						isiApiURL+publishApi+"/"+dposId+"/", 
						HttpMethod.DELETE, 
						requestDelete, 
						String.class);
		
//		System.err.println(deleteResponse.getBody());
	}

	@Test	
	public void testCreateMirko() throws IOException {

///////////////////////////////////////
// CREATE
///////////////////////////////////////

		
		String isiApiURL = "http://localhost:"+randomServerPort;
		
		String publishApi = "/v1/dpo";
//		small request
//		URL fileURL = this.getClass().getClassLoader().getResource("response.txt");
//		bigger request
//		URL fileURL = this.getClass().getClassLoader().getResource("full_tif.tif");
//		too big request
		URL fileURL = this.getClass().getClassLoader().getResource("marx-geo.tar.gz");
		
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> jsonMap = mapper.readValue(testMetadata,
		    new TypeReference<Map<String,Object>>(){});
		
		String plainCreds = "user:password";
		byte[] plainCredsBytes = plainCreds.getBytes();
		byte[] base64CredsBytes = Base64.getEncoder().encode(plainCredsBytes);
		String base64Creds = new String(base64CredsBytes);

//		RequestContainer container = new RequestContainer();
//		RequestElement el = new RequestElement();
//		
//		RequestAttributes attr = new RequestAttributes();
//		attr.setAttributeId("ns:c3isp:dpo-metadata");
//		attr.setValue(mapper.writeValueAsString(jsonMap));
//		
//		RequestAttributes dsaIdAttr = new RequestAttributes();
//		dsaIdAttr.setAttributeId("ns:c3isp:dsa-id");
//		dsaIdAttr.setValue(dsaId);
//		
//		
//		LinkedList<RequestAttributes> list = new LinkedList<RequestAttributes>();
//		
//		
//		list.add(attr);
//		list.add(dsaIdAttr);
//		
//		el.setAttributes(list);
//		container.setRequest(el);
		
		System.err.println("create metadata");
		System.err.println(mapper.writeValueAsString(mapper.readTree(testMetadataMirko)));
		System.err.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(mapper.readTree(testMetadataMirko)));
		
		MultiValueMap<String, Object> parts = new LinkedMultiValueMap<String, Object>();

		parts.add("input_metadata", testMetadataMirko);
		parts.add("fileToSubmit", new FileSystemResource(new File(fileURL.getFile())) );
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Basic " + base64Creds);

		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		HttpEntity<MultiValueMap<String, Object>> request = 
				new HttpEntity<MultiValueMap<String, Object>>(parts, headers);
		
		System.err.println(headers.getContentType());
		
//		TestRestTemplate restTemplate = new TestRestTemplate(httpClientOptions)
//		TestRestTemplate restTemplate = new TestRestTemplate(createSSLIgnoringRestTemplate());

		RestTemplate restTemplate = TestUtils.createSSLIgnoringRestTemplate();		
		
		ResponseEntity<RestResponse> response = restTemplate//.withBasicAuth("user", "password")
				.exchange(isiApiURL+publishApi, HttpMethod.POST, request, RestResponse.class);
		
		RestResponse respBody = response.getBody();
		
		String dposId = respBody.getAdditionalProperties().get("dposId");
		
		System.err.println("dpo_id is: " + dposId);
		
///////////////////////////////////////
		// READ
///////////////////////////////////////
		
		MultiValueMap<String, Object> parts1 = new LinkedMultiValueMap<String, Object>();

//		parts.add("input_metadata", container);
//		parts.add("fileToSubmit", new FileSystemResource(new File(fileURL.getFile())) );
		
	
		
		HttpHeaders headers1 = new HttpHeaders();
		headers1.add("Authorization", "Basic " + base64Creds);
//		headers1.add("X-c3isp-input_metadata", URLEncoder.encode("{}", "UTF-8"));
		headers1.add("X-c3isp-input_metadata", "{}");
			
//		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		HttpEntity<MultiValueMap<String, Object>> request1 = 
				new HttpEntity<MultiValueMap<String, Object>>(parts1, headers1);
		
		System.err.println(request1);
		
//		ResponseEntity<FileSystemResource> readResponse = restTemplate//.withBasicAuth("user", "password")
//				.exchange(
//						isiApiURL+publishApi+"/"+dposId+"/", 
//						HttpMethod.GET, 
//						request1, 
//						FileSystemResource.class);
		
		System.err.println("read metadata");
		
		
		ResponseEntity<ByteArrayResource> readResponse = restTemplate//.withBasicAuth("user", "password")
		.exchange(
				isiApiURL+publishApi+"/"+dposId+"/", 
				HttpMethod.GET, 
				request1, 
				ByteArrayResource.class);
		
		
		ByteArrayResource file = readResponse.getBody(); 
		File tmpFile = File.createTempFile("udd", ".fff");
		FileUtils.forceDeleteOnExit(tmpFile);
		FileUtils.copyInputStreamToFile(file.getInputStream(), tmpFile);
		assertTrue(FileUtils.contentEquals(tmpFile, new File(fileURL.getFile())));
		
//		ResponseEntity<String> deleteResponse = 
//				restTemplate.delete(
//				isiApiURL+publishApi+"/"+dposId+"/");
		
		
		
/// DELETE
		
		MultiValueMap<String, Object> partsDelete = new LinkedMultiValueMap<String, Object>();
		
	
		
		HttpHeaders headersDelete = new HttpHeaders();
		headersDelete.add("Authorization", "Basic " + base64Creds);
		headersDelete.add("X-c3isp-input_metadata", "{}");
			
		headersDelete.setContentType(MediaType.APPLICATION_JSON_UTF8);
		HttpEntity<MultiValueMap<String, Object>> requestDelete = 
				new HttpEntity<MultiValueMap<String, Object>>(partsDelete, headersDelete);

		
		
		ResponseEntity<String> deleteResponse = restTemplate//.withBasicAuth("user", "password")
				.exchange(
						isiApiURL+publishApi+"/"+dposId+"/", 
						HttpMethod.DELETE, 
						requestDelete, 
						String.class);
		
//		System.err.println(deleteResponse.getBody());
	}

	
}
