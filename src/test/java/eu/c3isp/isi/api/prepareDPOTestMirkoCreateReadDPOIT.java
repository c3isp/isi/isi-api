package eu.c3isp.isi.api;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.Base64;
import java.util.LinkedList;
import java.util.Map;
import java.util.Vector;

import javax.net.ssl.SSLContext;

import org.apache.commons.io.FileUtils;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.tomcat.util.digester.DocumentProperties.Charset;
//import org.apache.commons.collections.map.MultiValueMap;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.cnr.iit.isi.api.restapi.impl.ApiCreateDPO;
import it.cnr.iit.isi.api.restapi.impl.ApiPrepareDPO;
import it.cnr.iit.isi.api.restapi.types.RestResponse;
import it.cnr.iit.isi.api.restapi.types.xacml.RequestAttributes;
import it.cnr.iit.isi.api.restapi.types.xacml.RequestContainer;
import it.cnr.iit.isi.api.restapi.types.xacml.RequestElement;

@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)

@ActiveProfiles("test") // load application-test.properties
@AutoConfigureMockMvc
@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class prepareDPOTestMirkoCreateReadDPOIT {
	
	private static final String INPUT_FILE = 
//			"mirko-dpocontent.cef";
			"mirko-stix.cef";
//			"2018-11-27T161723.000+0100.fgt";
			
	String test = ApiCreateDPO.EXAMPLERESTMETADATA;
	
//	String testDPO_ID_Obligations = //"1567176315082-35bd5add-9f90-4d34-894b-a1c0646246bf";
//			"1567416054695-0b3cb26c-9d3e-4aae-9cda-f6cc3204fe24";
//			"1567605009640-f6f62941-7b08-4f79-b04a-43da5c49448b";
//			"1567605534282-5c5aeeb8-4135-45d2-a163-85a4ae20912b";
//			"1567612699076-1da002d8-ad69-4d18-bbd1-3dd22f3d77b5";
			
	
			
//	String old_dpo = "1567416054695-0b3cb26c-9d3e-4aae-9cda-f6cc3204fe24";
	
//	String testDPO_ID_DSA_ID = "DSA-6b0ee779-3f8d-443b-99bc-510672b44090";
				
	
	String DSA_ID = //"DSA-44ee3dfc-4d3a-4219-979a-eb6ba7a8efc2";
//			"DSA-e3d5bc24-18dd-41f1-ba6c-5f7992fb71d0";
//			"DSA-1f6fe910-f595-4c34-8b9c-837202db5f3a";
			"DSA-6b0ee779-3f8d-443b-99bc-510672b44090";
	
	String testMetadata = "{            \"id\" : \"4000123\",       \"dsa_id\" :  \""
			+ DSA_ID
			+ "\",       \"start_time\" : \"2017-12-14T12:00:00.0Z\",       \"end_time\" : \"2017-12-14T18:01:01.0Z\",       \"event_type\" : \"Firewall Event\",       \"organization\" : \"3DRepo\" }";
	
	
//	String testMetadata  = "";

	String testMetadataFull = "{\"Request\":{\"Attribute\":[{\"AttributeId\":\"urn:oasis:names:tc:xacml:1.0:subject:subject-id\",\"Value\":\"user2\",\"DataType\":\"string\"},{\"AttributeId\":\"urn:oasis:names:tc:xacml:1.0:subject:authentication-type\",\"Value\":\"low\",\"DataType\":\"string\"},{\"AttributeId\":\"urn:oasis:names:tc:xacml:1.0:action:action-id\",\"Value\":\"read\",\"DataType\":\"string\"},{\"AttributeId\":\"urn:oasis:names:tc:xacml:3.0:subject:access-purpose\",\"Value\":\"Cyber Threat Monitoring\",\"DataType\":\"string\"},{\"AttributeId\":\"ns:c3isp:aggregation-operation\",\"Value\":\"empty\",\"DataType\":\"string\"}]}}";
	
	
	String prepareDataAPI = "/v1/prepareData";
	
	public static final String  IAI_SESSION_ID = "urn:oasis:names:tc:xacml:1.0:action:iai:session-id";

	
	@Value("${isi.api.url}")
	String isiApiURL;
	
	@Autowired
	TestRestTemplate restTemplate;
	
	
	
	
//	@Test
	public void testgetDSAUsagePolicy() {
		
		RestTemplate restTemplate = TestUtils.createSSLIgnoringRestTemplate();
		
		String dsaMgrURL  = "https://dsamgrc3isp.iit.cnr.it/DSAAPI";

		String getPolicyEndpoint = "/fetchusagepolicy";

		String dsa = "DSA-1f6fe910-f595-4c34-8b9c-837202db5f3a";

		String plainCreds = "user:password";
		byte[] plainCredsBytes = plainCreds.getBytes();
		byte[] base64CredsBytes = Base64.getEncoder().encode(plainCredsBytes);
		String base64Creds = new String(base64CredsBytes);

		
		URI address = UriComponentsBuilder.fromUriString(dsaMgrURL)
				.path(getPolicyEndpoint)
				.pathSegment(dsa)
				.build()
				.toUri();		
 
		

		MultiValueMap<String, Object> parts = new LinkedMultiValueMap<String, Object>();

		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Basic " + base64Creds);

		HttpEntity<MultiValueMap<String, Object>> request = 
				new HttpEntity<MultiValueMap<String, Object>>(parts, headers);

		System.err.println(headers.getContentType());
		ResponseEntity<String> response = null;



		response = restTemplate//.withBasicAuth("user", "password")
				.exchange(address, HttpMethod.GET, request, String.class);

		String respBody = response.getBody();

		System.err.println("response is: " + respBody);

	}
	
	
	protected String createISIAPIjson (ObjectMapper mapper) throws JsonProcessingException {
		RequestContainer read_container = new RequestContainer();
		RequestElement el = new RequestElement();

		RequestAttributes attr = new RequestAttributes();
		attr.setAttributeId("urn:oasis:names:tc:xacml:3.0:subject:access-purpose");
//		attr.setValue("generic_test_purpose");
		attr.setValue("Cyber Threat Monitoring");
//		attr.setValue("Data Breaches Notification");
		
		RequestAttributes attr2 = new RequestAttributes();
		attr2.setAttributeId(IAI_SESSION_ID);
		attr2.setValue("111999111"+System.currentTimeMillis()+"999111999");
		
		RequestAttributes authUA = new RequestAttributes();
		authUA.setAttributeId("urn:oasis:names:tc:xacml:1.0:subject:subject-id");
		authUA.setValue("user2");
		
		RequestAttributes aggrA = new RequestAttributes();
		aggrA.setAttributeId("ns:c3isp:aggregation-operation");
		aggrA.setValue("empty");
		
		RequestAttributes aggrAct = new RequestAttributes();
		aggrAct.setAttributeId("urn:oasis:names:tc:xacml:1.0:action:action-id");
		aggrAct.setValue("read");

		LinkedList<RequestAttributes> list = new LinkedList<RequestAttributes>();

		list.add(attr);
		list.add(attr2);
		list.add(authUA);
		list.add(aggrA);
		list.add(aggrAct);

		
		el.setAttributes(list);
		read_container.setRequest(el);
		
		return mapper.writeValueAsString(read_container);
	}
	
	
	
	@Test	
	public void testPrepareDPO() throws IOException {

	Vector<String> dpos = new Vector<String>();	
		
		
///////////////////////////////////////
// CREATE
///////////////////////////////////////

		
		String publishApi = "/v1/dpo";
		
		URL fileURL = this.getClass().getClassLoader().getResource(INPUT_FILE);

		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> jsonMap = mapper.readValue(testMetadata,
		    new TypeReference<Map<String,Object>>(){});
		
		String plainCreds = "user:password";
		byte[] plainCredsBytes = plainCreds.getBytes();
		byte[] base64CredsBytes = Base64.getEncoder().encode(plainCredsBytes);
		String base64Creds = new String(base64CredsBytes);

		RequestContainer container = new RequestContainer();
		RequestElement el = new RequestElement();
		RequestAttributes attr = new RequestAttributes();
		attr.setAttributeId("ns:c3isp:dpo-metadata");
		attr.setValue(mapper.writeValueAsString(jsonMap));
		
		RequestAttributes dsaIdAttr = new RequestAttributes();
		dsaIdAttr.setAttributeId("ns:c3isp:dsa-id");
		dsaIdAttr.setValue(DSA_ID);

		LinkedList<RequestAttributes> list = new LinkedList<RequestAttributes>();
		list.add(attr);
		el.setAttributes(list);
		container.setRequest(el);
		
		System.err.println("create metadata");
		System.err.println(mapper.writeValueAsString(container));
		System.err.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(container));
		
		MultiValueMap<String, Object> parts = new LinkedMultiValueMap<String, Object>();

		parts.add("input_metadata", container);
		parts.add("fileToSubmit", new FileSystemResource(new File(fileURL.getFile())) );
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Basic " + base64Creds);

		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		HttpEntity<MultiValueMap<String, Object>> request = 
				new HttpEntity<MultiValueMap<String, Object>>(parts, headers);
		
		System.err.println(headers.getContentType());
		
//		TestRestTemplate restTemplate = new TestRestTemplate(httpClientOptions)
//		TestRestTemplate restTemplate = new TestRestTemplate(createSSLIgnoringRestTemplate());

		RestTemplate restTemplate = TestUtils.createSSLIgnoringRestTemplate();		

		ResponseEntity<RestResponse> response = null;
		
		for (int i = 0; i < 4; i++) {

			response = restTemplate//.withBasicAuth("user", "password")
					.exchange(isiApiURL+publishApi, HttpMethod.POST, request, RestResponse.class);

			RestResponse respBody = response.getBody();

			String dposId = respBody.getAdditionalProperties().get("dposId");

			System.err.println("dpo_id is: " + dposId);

			dpos.add(dposId);

		}
//		
/////////////////////////////////////////
//		call prepareData in ISI API
/////////////////////////////////////////

		URI address = UriComponentsBuilder.fromUriString(isiApiURL)
				.path(prepareDataAPI)
				.queryParam("data_lake", "VDL")
				.queryParam("format", "CEF") // ignored so far
				.queryParam("serviceName", "test_prepareData")
				.queryParam("type", "FS")
				.build()
				.toUri();		

		
				
		LinkedList<String> dpoList = new LinkedList<String>(); 
				
//		dpoList.add("\""+dposId+"\"");
//		dpoList.add("\""+dposId2+"\"");

		for (String dpoId : dpos) {
			dpoList.add(dpoId);
		}
		
		
		/**
		 * TODO check here, adding Mirko's DPO
		 */
		
//		dpoList.add(testDPO_ID_Obligations);
		
//		MultiValueMap<String, Object> parts1 = new LinkedMultiValueMap<String, Object>();

//		parts1.add("DPO_id_list", dpoList);
		
		MultiValueMap<String, String> headers1 = new LinkedMultiValueMap<String, String>();
		headers1.add("Authorization", "Basic " + base64Creds);
		headers1.add("Content-Type", "application/json");
		headers1.add("X-c3isp-input_metadata", createISIAPIjson(mapper));
		headers1.add("X-c3isp-metadata", createISIAPIjson(mapper));
		
		HttpEntity<LinkedList<String>> requestSpec = new HttpEntity<LinkedList<String>>(dpoList, headers1);

		
		
//		HttpEntity<MultiValueMap<String, Object>> request1 = 
//				new HttpEntity<MultiValueMap<String, Object>>(parts1, headers1);
		
		System.err.println("size of messageconverters before: "+restTemplate.getMessageConverters().size());
		
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

		System.err.println("size of messageconverters after: "+restTemplate.getMessageConverters().size());
		
		
		System.err.println("requestSpec is: "+requestSpec.toString());
		
		System.err.println("call isi api preparedata");
		
		
//		ResponseEntity<String> readResponse = restTemplate//.withBasicAuth("user", "password")
//		.exchange(
////				isiApiURL+publishApi+"/"+dposId+"/",
//				address,
//				HttpMethod.POST, 
//				request1, 
//				String.class);

		String readResponse = restTemplate
				.postForObject(address, requestSpec, String.class);
				
				
//		System.err.println("buffer manager response: status="+readResponse.getStatusCodeValue());
//		System.err.println("buffer manager response: value="+readResponse.getBody());
	
//		System.err.println("buffer manager response: status="+readResponse.getStatusCodeValue());
		System.err.println("isi api prepareData response: value="+readResponse);
		
/**
 * 
 * NOT OK: DMO test on read may be ok if and only if the DMO is prescribed at the creation
 * if only at the read, then the DMO-modified content is in the data lake and there 
 * is no easy way to read it from this test 
 * 
///////////////////////////////////////
// READ
///////////////////////////////////////

		MultiValueMap<String, Object> parts1 = new LinkedMultiValueMap<String, Object>();

		//parts.add("input_metadata", container);
		//parts.add("fileToSubmit", new FileSystemResource(new File(fileURL.getFile())) );



		HttpHeaders headersRead = new HttpHeaders();
		headersRead.add("Authorization", "Basic " + base64Creds);
		//headers1.add("X-c3isp-input_metadata", URLEncoder.encode("{}", "UTF-8"));
		headersRead.add("X-c3isp-input_metadata", "{}");

		//headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		HttpEntity<MultiValueMap<String, Object>> requestRead = 
				new HttpEntity<MultiValueMap<String, Object>>(parts1, headersRead);

		System.err.println(requestRead);

		//ResponseEntity<FileSystemResource> readResponse = restTemplate//.withBasicAuth("user", "password")
		//.exchange(
		//isiApiURL+publishApi+"/"+dposId+"/", 
		//HttpMethod.GET, 
		//request1, 
		//FileSystemResource.class);

		System.err.println("read metadata");


		ResponseEntity<ByteArrayResource> readTResponse = restTemplate//.withBasicAuth("user", "password")
				.exchange(
						isiApiURL+publishApi+"/"+dpos.get(0)+"/", 
						HttpMethod.GET, 
						requestRead, 
						ByteArrayResource.class);


		
		
		ByteArrayResource file = readTResponse.getBody(); 
		File tmpFile = File.createTempFile("udd", ".fff");
		FileUtils.forceDeleteOnExit(tmpFile);
		FileUtils.copyInputStreamToFile(file.getInputStream(), tmpFile);
		
		// we expect the DMO to have changed the file content
		// *** HOWEVER this is only true at the create, result should be checked out of the data lake***
		assertTrue(!FileUtils.contentEquals(tmpFile, new File(fileURL.getFile())));
		
		assertTrue(
				FileUtils.readFileToString(
						new File(fileURL.getFile()), 
						StandardCharsets.UTF_8).contains("146.48.99.68")
				);

		assertTrue(
				!
				FileUtils.readFileToString(
						tmpFile, 
						StandardCharsets.UTF_8).contains("146.48.99.68")
				);
		
//		System.err.println(				FileUtils.readFileToString(
//						tmpFile, 
//						StandardCharsets.UTF_8));
*/		
		
		
/// DELETE
		
		MultiValueMap<String, Object> partsDelete = new LinkedMultiValueMap<String, Object>();
		
	
		
		HttpHeaders headersDelete = new HttpHeaders();
		headersDelete.add("Authorization", "Basic " + base64Creds);
		headersDelete.add("X-c3isp-input_metadata", "{}");
			
		headersDelete.setContentType(MediaType.APPLICATION_JSON_UTF8);
		HttpEntity<MultiValueMap<String, Object>> requestDelete = 
				new HttpEntity<MultiValueMap<String, Object>>(partsDelete, headersDelete);

		
		
		for (String dposId : dpos) {
		
			
			ResponseEntity<String> deleteResponse = restTemplate//.withBasicAuth("user", "password")
					.exchange(
							isiApiURL+publishApi+"/"+dposId+"/", 
							HttpMethod.DELETE, 
							requestDelete, 
							String.class);

			System.err.println("dposId: "+dposId+" : "+deleteResponse.getBody());
		}
		
//		System.err.println(deleteResponse.getBody());
	}
	
	

//	@Test	
//	public void testPrepareDPOfromReadToTestDMO() throws IOException {
//
//	Vector<String> dpos = new Vector<String>();	
//		
//
//	String publishApi = "/v1/dpo";
//
//	
//	String plainCreds = "user:password";
//	byte[] plainCredsBytes = plainCreds.getBytes();
//	byte[] base64CredsBytes = Base64.getEncoder().encode(plainCredsBytes);
//	String base64Creds = new String(base64CredsBytes);
//
//	RestTemplate restTemplate = TestUtils.createSSLIgnoringRestTemplate();		
//	ObjectMapper mapper = new ObjectMapper();
//	
//	MultiValueMap<String, Object> parts = new LinkedMultiValueMap<String, Object>();
//	
//
//	
//	// sets up the DPO_ID for testing
////	dpos.add(old_dpo);
//	dpos.add(testDPO_ID_Obligations);
//	dpos.add(testDPO_ID_Obligations);
//	dpos.add(testDPO_ID_Obligations);
//
//		
///////////////////////////////////////////
////		call prepareData in ISI API
///////////////////////////////////////////
//
//		URI address = UriComponentsBuilder.fromUriString(isiApiURL)
//				.path(prepareDataAPI)
//				.queryParam("data_lake", "VDL")
//				.queryParam("format", "CEF") // ignored so far
//				.queryParam("serviceName", "test_prepareData")
//				.queryParam("type", "FS")
//				.build()
//				.toUri();		
//
//		
//				
//		LinkedList<String> dpoList = new LinkedList<String>(); 
//				
////		dpoList.add("\""+dposId+"\"");
////		dpoList.add("\""+dposId2+"\"");
//
//		for (String dpoId : dpos) {
//			dpoList.add(dpoId);
//		}
//		
//		
////		MultiValueMap<String, Object> parts1 = new LinkedMultiValueMap<String, Object>();
//
////		parts1.add("DPO_id_list", dpoList);
//		
//		MultiValueMap<String, String> headers1 = new LinkedMultiValueMap<String, String>();
//		headers1.add("Authorization", "Basic " + base64Creds);
//		headers1.add("Content-Type", "application/json");
//		headers1.add("X-c3isp-input_metadata", createISIAPIjson(mapper));
//		headers1.add("X-c3isp-metadata", createISIAPIjson(mapper));
//		
//		HttpEntity<LinkedList<String>> requestSpec = new HttpEntity<LinkedList<String>>(dpoList, headers1);
//
//		
//		
////		HttpEntity<MultiValueMap<String, Object>> request1 = 
////				new HttpEntity<MultiValueMap<String, Object>>(parts1, headers1);
//		
//		System.err.println("size of messageconverters before: "+restTemplate.getMessageConverters().size());
//		
//		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
//
//		System.err.println("size of messageconverters after: "+restTemplate.getMessageConverters().size());
//		
//		
//		System.err.println("requestSpec is: "+requestSpec.toString());
//		
//		System.err.println("call isi api preparedata");
//		
//		
////		ResponseEntity<String> readResponse = restTemplate//.withBasicAuth("user", "password")
////		.exchange(
//////				isiApiURL+publishApi+"/"+dposId+"/",
////				address,
////				HttpMethod.POST, 
////				request1, 
////				String.class);
//
//		String readResponse = restTemplate
//				.postForObject(address, requestSpec, String.class);
//				
//				
////		System.err.println("buffer manager response: status="+readResponse.getStatusCodeValue());
////		System.err.println("buffer manager response: value="+readResponse.getBody());
//	
////		System.err.println("buffer manager response: status="+readResponse.getStatusCodeValue());
//		System.err.println("isi api prepareData response: value="+readResponse);
//		
//
//		
///// DELETE
//		
////		MultiValueMap<String, Object> partsDelete = new LinkedMultiValueMap<String, Object>();
////		
////	
////		
////		HttpHeaders headersDelete = new HttpHeaders();
////		headersDelete.add("Authorization", "Basic " + base64Creds);
////		headersDelete.add("X-c3isp-input_metadata", "{}");
////			
////		headersDelete.setContentType(MediaType.APPLICATION_JSON_UTF8);
////		HttpEntity<MultiValueMap<String, Object>> requestDelete = 
////				new HttpEntity<MultiValueMap<String, Object>>(partsDelete, headersDelete);
////
////		
////		
////		for (String dposId : dpos) {
////		
////			
////			ResponseEntity<String> deleteResponse = restTemplate//.withBasicAuth("user", "password")
////					.exchange(
////							isiApiURL+publishApi+"/"+dposId+"/", 
////							HttpMethod.DELETE, 
////							requestDelete, 
////							String.class);
////
////			System.err.println("dposId: "+dposId+" : "+deleteResponse.getBody());
////		}
////		
////		System.err.println(deleteResponse.getBody());
//	}
//	

	
	
	
}
