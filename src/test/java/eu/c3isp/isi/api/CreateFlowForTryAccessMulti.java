package eu.c3isp.isi.api;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.Base64;
import java.util.LinkedList;
import java.util.Map;

import javax.net.ssl.SSLContext;

import org.apache.commons.io.FileUtils;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.tomcat.util.digester.DocumentProperties.Charset;
//import org.apache.commons.collections.map.MultiValueMap;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StreamUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.cnr.iit.isi.api.restapi.impl.ApiCreateDPO;
import it.cnr.iit.isi.api.restapi.types.RestResponse;
import it.cnr.iit.isi.api.restapi.types.xacml.RequestAttributes;
import it.cnr.iit.isi.api.restapi.types.xacml.RequestContainer;
import it.cnr.iit.isi.api.restapi.types.xacml.RequestElement;

@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)

@ActiveProfiles("test") // load application-test.properties
@AutoConfigureMockMvc
@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class CreateFlowForTryAccessMulti {
	
	String test = ApiCreateDPO.EXAMPLERESTMETADATA;
	String testMetadata = "{            \"id\" : \"4000123\",       \"dsa_id\" :  \"DSA-56976731-3c16-46cc-a4e1-8384c6208eb0\",       \"start_time\" : \"2017-12-14T12:00:00.0Z\",       \"end_time\" : \"2017-12-14T18:01:01.0Z\",       \"event_type\" : \"Firewall Event\",       \"organization\" : \"3DRepo\" }";
	
	String testOnlyDPOMetadataMirkoOld = "{\"id\":\"4000123\",\"dsa_id\":\"DSA-56976731-3c16-46cc-a4e1-8384c6208eb0\",\"start_time\":\"2017-12-14T12:00:00.0Z\",\"end_time\":\"2017-12-14T18:01:01.0Z\",\"event_type\":\"Firewall Event\",\"organization\":\"3DRepo\"}";
	String testOnlyDPOMetadataMirko = "{\"id\":\"4000123\",\"dsa_id\":\"DSA-c7aa89e7-ed98-42a8-91cf-74876c7ebc3e\",\"start_time\":\"2017-12-14T12:00:00.0Z\",\"end_time\":\"2017-12-14T18:01:01.0Z\",\"event_type\":\"Firewall Event\",\"organization\":\"3DRepo\"}"; 
	
	
//	String testMetadataMirko = "{\"Request\":{\"Attribute\":[{\"AttributeId\":\"ns:c3isp:dpo-metadata\",\"Value\":\"{\\\"id\\\":\\\"4000123\\\",\\\"dsa_id\\\":\\\"DSA-56976731-3c16-46cc-a4e1-8384c6208eb0\\\",\\\"start_time\\\":\\\"2017-12-14T12:00:00.0Z\\\",\\\"end_time\\\":\\\"2017-12-14T18:01:01.0Z\\\",\\\"event_type\\\":\\\"Firewall Event\\\",\\\"organization\\\":\\\"3DRepo\\\"}\",\"DataType\":\"string\"}]}}";
	// with DSA-id updated on 20181207
	String testMetadataMirko = "{\"Request\":{\"Attribute\":[{\"AttributeId\":\"ns:c3isp:dpo-metadata\",\"Value\":\"{\\\"id\\\":\\\"4000123\\\",\\\"dsa_id\\\":\\\"DSA-c7aa89e7-ed98-42a8-91cf-74876c7ebc3e\\\",\\\"start_time\\\":\\\"2017-12-14T12:00:00.0Z\\\",\\\"end_time\\\":\\\"2017-12-14T18:01:01.0Z\\\",\\\"event_type\\\":\\\"Firewall Event\\\",\\\"organization\\\":\\\"3DRepo\\\"}\",\"DataType\":\"string\"}]}}";
	
	String testMetadataMirkoWrong = "{\"Request\":{\"Attribute\":[{\"AttributeId\":\"ns:c3isp:dpo-metadata\",\"Value\":{\"id\":\"4000123\",\"dsa_id\":\"DSA-56976731-3c16-46cc-a4e1-8384c6208eb0\",\"start_time\":\"2017-12-14T12:00:00.0Z\",\"end_time\":\"2017-12-14T18:01:01.0Z\",\"event_type\":\"Firewall Event\",\"organization\":\"3DRepo\"},\"DataType\":\"string\"}]}}";
	
//	String dsaId = "DSA-56976731-3c16-46cc-a4e1-8384c6208eb0";
	
	String dsaId  = //"DSA-66749812-67b8-4e47-beb0-55fd99b835be";
			"DSA-c7aa89e7-ed98-42a8-91cf-74876c7ebc3e";
	
	
	String prepareEmptyDataLakeAPI = "/v1/prepareEmptyDataLake";
	String publishApi = "/v1/dpo";
	String prepareDataApi = "/v1/prepareData";
	String releaseVDL = "/v1/releaseData"; 
	String copyInVDL = "/v1/populateDataLake";
	
	
	@Value("${isi.api.url}")
	String isiApiURL;
	
	@Autowired
	TestRestTemplate restTemplate;
	
	@Value("${buffer.manager.test.url}")
	private String bufferManagerUrl;
	
	@Test	
	public void testCreate() throws IOException {

///////////////////////////////////////
// CREATE
///////////////////////////////////////

		
		
		URL fileURL = this.getClass().getClassLoader().getResource("DataLake_MalwareEvents.csv");

		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> jsonMap = mapper.readValue(testOnlyDPOMetadataMirko,
		    new TypeReference<Map<String,Object>>(){});
		
		String plainCreds = "user:password";
		byte[] plainCredsBytes = plainCreds.getBytes();
		byte[] base64CredsBytes = Base64.getEncoder().encode(plainCredsBytes);
		String base64Creds = new String(base64CredsBytes);

		RequestContainer container = new RequestContainer();
		RequestElement el = new RequestElement();
		
		RequestAttributes attr = new RequestAttributes();
		attr.setAttributeId("ns:c3isp:dpo-metadata");
		attr.setValue(mapper.writeValueAsString(jsonMap));
		
		RequestAttributes dsaIdAttr = new RequestAttributes();
		dsaIdAttr.setAttributeId("ns:c3isp:dsa-id");
		dsaIdAttr.setValue(dsaId);
		
		
		LinkedList<RequestAttributes> list = new LinkedList<RequestAttributes>();
		
		
		list.add(attr);
		list.add(dsaIdAttr);
		
		el.setAttributes(list);
		container.setRequest(el);
		
		System.err.println("create metadata");
		System.err.println(mapper.writeValueAsString(container));
		System.err.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(container));
		
		MultiValueMap<String, Object> parts = new LinkedMultiValueMap<String, Object>();

		parts.add("input_metadata", container);
		
		File tobeSent = new File(fileURL.getFile());
		
		System.err.println(tobeSent.getName());
		
		parts.add("fileToSubmit", new FileSystemResource(tobeSent) );
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Basic " + base64Creds);

		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		HttpEntity<MultiValueMap<String, Object>> request = 
				new HttpEntity<MultiValueMap<String, Object>>(parts, headers);
		
		System.err.println(headers.getContentType());
		System.err.println(request);
		
//		TestRestTemplate restTemplate = new TestRestTemplate(httpClientOptions)
//		TestRestTemplate restTemplate = new TestRestTemplate(createSSLIgnoringRestTemplate());

		RestTemplate restTemplate = TestUtils.createSSLIgnoringRestTemplate();		
		
		ResponseEntity<RestResponse> response = restTemplate//.withBasicAuth("user", "password")
				.exchange(isiApiURL+publishApi, HttpMethod.POST, request, RestResponse.class);
		
		RestResponse respBody = response.getBody();
		
		String dposId = respBody.getAdditionalProperties().get("dposId");
		
		System.err.println("dpo_id is: " + dposId);
		
		
///////////////////////////////////////
// 2nd Create
///////////////////////////////////////
		
		fileURL = this.getClass().getClassLoader().getResource("DataLake_MalwareEvents2.csv");

		parts.remove("fileToSubmit");
		parts.add("fileToSubmit", new FileSystemResource(tobeSent) );
		
		ResponseEntity<RestResponse> response2 = restTemplate//.withBasicAuth("user", "password")
				.exchange(isiApiURL+publishApi, HttpMethod.POST, request, RestResponse.class);
		
		respBody = response.getBody();
		
		String dposId2 = respBody.getAdditionalProperties().get("dposId");
				
		
///////////////////////////////////////
// Buffer Manager prepareData MySQL
///////////////////////////////////////
		
//		String prepareDataCommand = "{\n" + 
//				"  \"DPO_id_list\": [\n" + 
//				dposId +","+ dposId2 + 
//				"  ],\n" + 
//				"  \"metadata\": \"{}\",\n" + 
//				"  \"data_lake\": \"VDL\",\n" + 
//				"  \"format\": \"CEF\",\n" + 
//				"  \"service_name\": \"matchDGA\",\n" + 
//				"  \"type\": \"MYSQL\"\n" + 
//				"}";
//		
//		
//		
//		
//		
//		URI address = UriComponentsBuilder.fromUriString(bufferManagerUrl)
//				.path(prepareDataApi)
//				.queryParam("data_lake", "VDL")
//				.queryParam("format", "CEF") // ignored so far
//				.queryParam("service_name", "test_anonymization")
//				.queryParam("type", "MYSQL")
//				.build()
//				.toUri();		

///////////////////////////////////////
//Buffer Manager createEmptyVDL FS
///////////////////////////////////////

		
		
		
		URI address = UriComponentsBuilder.fromUriString(bufferManagerUrl)
				.path(prepareEmptyDataLakeAPI)
				.queryParam("data_lake", "VDL")
				.queryParam("type", "FS")
				.build()
				.toUri();		

		
				
		LinkedList<String> dpoList = new LinkedList<String>(); 
				
//		dpoList.add("\""+dposId+"\"");
//		dpoList.add("\""+dposId2+"\"");

		dpoList.add(dposId);
		dpoList.add(dposId2);

		
		MultiValueMap<String, String> headers1 = new LinkedMultiValueMap<String, String>();
		headers1.add("Authorization", "Basic " + base64Creds);
		headers1.add("Content-Type", "application/json");

		HttpEntity<LinkedList<String>> requestSpec = new HttpEntity<LinkedList<String>>(headers1);

		
		
//		HttpEntity<MultiValueMap<String, Object>> request1 = 
//				new HttpEntity<MultiValueMap<String, Object>>(parts1, headers1);
		

		String readResponse = restTemplate
				.postForObject(address, requestSpec, String.class);
				
		JsonNode actualObj = mapper.readTree(readResponse);
		String dataLakeURI = actualObj.get("URI").asText(); 
		
		
				
		System.err.println("buffer manager response: value="+readResponse);
		System.err.println("buffer manager URI is: "+dataLakeURI);
		

///////////////////////////////////////
//READ API
///////////////////////////////////////
		
		// fake implementation, read is the one out of bundleManagerReadResponse
		
		
///////////////////////////////////////
//Copy in VDL
///////////////////////////////////////

		// if using populateDataLake
		
//		address = UriComponentsBuilder.fromUriString(bufferManagerUrl)
//				.path(copyInVDL)
//				.queryParam("uri", dataLakeURI)
//				.build()
//				.toUri();
//		
//		
//		
//		HttpEntity<FileSystemResource> requestReleaseSpec = new HttpEntity<FileSystemResource>(
//				new FileSystemResource(tobeSent), headers1);
//
//		
//		
//		String copyDatainVDLResponse = restTemplate
//				.postForObject(address, requestReleaseSpec, String.class);
//		
//		JsonNode responseObj = mapper.readTree(copyDatainVDLResponse);
//		String responseCopy = responseObj.get("code").asText();
		

		// if using file system operations
		
		
		
		
///////////////////////////////////////
//Release VDL
///////////////////////////////////////
		
		address = UriComponentsBuilder.fromUriString(bufferManagerUrl)
				.path(releaseVDL)
				.build()
				.toUri();		
		
		
//		LinkedList<String> dataLakeList = new LinkedList<String>();
//		
//		dataLakeList.add(dataLakeURI);
		
		HttpEntity<String> requestReleaseSpec = new HttpEntity<String>(dataLakeURI, headers1);

		
		
		String readReleaseDataResponse = restTemplate
				.postForObject(address, requestReleaseSpec, String.class);
				
		System.err.println("release DataLake answer is: "+ readReleaseDataResponse);
		
	}	
	
	protected String createISIAPIjson (ObjectMapper mapper) throws JsonProcessingException {
		RequestContainer read_container = new RequestContainer();
		RequestElement el = new RequestElement();

		RequestAttributes attr = new RequestAttributes();
		attr.setAttributeId("urn:oasis:names:tc:xacml:3.0:subject:access-purpose");
		attr.setValue("generic_test_purpose");

		LinkedList<RequestAttributes> list = new LinkedList<RequestAttributes>();

		list.add(attr);

		el.setAttributes(list);
		read_container.setRequest(el);
		
		return mapper.writeValueAsString(read_container) ;
	}
	
}
