package eu.c3isp.isi.api;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URI;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.Base64;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.Vector;

import javax.net.ssl.SSLContext;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.io.FileUtils;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.Test;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

public class prepareDPOTest_AnonymizationJonas {
	
	
	/**
	 * TODO fix here
	 */
	private static final String INPUT_FILE = //"mirko-dpocontent.cef";
 "francesco.cef";		

	private static final String INPUT_FILE_NETFLOW = //"mirko-dpocontent.cef";
			 "2018-11-14T121208.000+0100.fgt";	
	
	
	
//	String test = ApiCreateDPO.EXAMPLERESTMETADATA;
	
			
//	String old_dpo = "1567416054695-0b3cb26c-9d3e-4aae-9cda-f6cc3204fe24";
//	
//	String testDPO_ID_DSA_ID = "DSA-e3d5bc24-18dd-41f1-ba6c-5f7992fb71d0";
				
	
	String DSA_ID = 
			"DSA-b33b0e4c-0213-449e-9f12-27d36d2c8b2a";

	String DSA_ID_AnonymizeIpV4ToLocation =  
			"DSA-f6400893-7378-4b3b-8880-1c020de3c30f";
	
	
	String testMetadata = "{            \"id\" : \"4000123\",       \"dsa_id\" :  \""
			+ DSA_ID
			+ "\",       \"start_time\" : \"2017-12-14T12:00:00.0Z\",       \"end_time\" : \"2017-12-14T18:01:01.0Z\",       \"event_type\" : \"Firewall Event\",       \"organization\" : \"3DRepo\" }";

	
	

	
//	String testMetadata  = "";

//	String testMetadataFull = "{\"Request\":{\"Attribute\":[{\"AttributeId\":\"urn:oasis:names:tc:xacml:1.0:subject:subject-id\",\"Value\":\"user2\",\"DataType\":\"string\"},{\"AttributeId\":\"urn:oasis:names:tc:xacml:1.0:subject:authentication-type\",\"Value\":\"low\",\"DataType\":\"string\"},{\"AttributeId\":\"urn:oasis:names:tc:xacml:1.0:action:action-id\",\"Value\":\"read\",\"DataType\":\"string\"},{\"AttributeId\":\"urn:oasis:names:tc:xacml:3.0:subject:access-purpose\",\"Value\":\"Cyber Threat Monitoring\",\"DataType\":\"string\"},{\"AttributeId\":\"ns:c3isp:aggregation-operation\",\"Value\":\"empty\",\"DataType\":\"string\"}]}}";
	
	
	String prepareDataAPI = "/v1/prepareData";
	
	public static final String  IAI_SESSION_ID = "urn:oasis:names:tc:xacml:1.0:action:iai:session-id";

	
	String isiApiURL = "https://isic3isp.iit.cnr.it:8443/isi-api";
	
	public static RestTemplate createSSLIgnoringRestTemplate() {
		TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;

		SSLContext sslContext;
		try {
			sslContext = org.apache.http.ssl.SSLContexts.custom()
			        .loadTrustMaterial(null, acceptingTrustStrategy)
			        .build();
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

		SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);

		CloseableHttpClient httpClient = HttpClients.custom()
		        .setSSLSocketFactory(csf)
		        .build();

		HttpComponentsClientHttpRequestFactory requestFactory =
		        new HttpComponentsClientHttpRequestFactory();

		requestFactory.setHttpClient(httpClient);

		RestTemplate restTemplate = new RestTemplate(requestFactory);
		
		return restTemplate;
	}


	
	protected String createISIAPIjson (ObjectMapper mapper) throws JsonProcessingException {
		RequestContainer read_container = new RequestContainer();
		RequestElement el = new RequestElement();

		RequestAttributes attr = new RequestAttributes();
		attr.setAttributeId("urn:oasis:names:tc:xacml:3.0:subject:access-purpose");
//		attr.setValue("generic_test_purpose");
		attr.setValue("Cyber Threat Monitoring");
//		attr.setValue("Data Breaches Notification");
		
		RequestAttributes attr2 = new RequestAttributes();
		attr2.setAttributeId(IAI_SESSION_ID);
		attr2.setValue("111999111"+System.currentTimeMillis()+"999111999");
		
		RequestAttributes authUA = new RequestAttributes();
		authUA.setAttributeId("urn:oasis:names:tc:xacml:1.0:subject:subject-id");
		authUA.setValue("user2");
		
		RequestAttributes aggrA = new RequestAttributes();
		aggrA.setAttributeId("ns:c3isp:aggregation-operation");
		aggrA.setValue("empty");
		
		RequestAttributes aggrAct = new RequestAttributes();
		aggrAct.setAttributeId("urn:oasis:names:tc:xacml:1.0:action:action-id");
		aggrAct.setValue("read");

		LinkedList<RequestAttributes> list = new LinkedList<RequestAttributes>();

		list.add(attr);
		list.add(attr2);
		list.add(authUA);
		list.add(aggrA);
		list.add(aggrAct);

		
		el.setAttributes(list);
		read_container.setRequest(el);
		
		return mapper.writeValueAsString(read_container);
	}
	
	
	
	@Test	
	public void testAnonymizationIpV4Randomization() throws IOException {

	Vector<String> dpos = new Vector<String>();	
		
		
///////////////////////////////////////
// CREATE
///////////////////////////////////////

		
		String publishApi = "/v1/dpo";
		
		URL fileURL = this.getClass().getClassLoader().getResource(INPUT_FILE);

		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> jsonMap = mapper.readValue(testMetadata,
		    new TypeReference<Map<String,Object>>(){});
		
		String plainCreds = "user:password";
		byte[] plainCredsBytes = plainCreds.getBytes();
		byte[] base64CredsBytes = Base64.getEncoder().encode(plainCredsBytes);
		String base64Creds = new String(base64CredsBytes);

		RequestContainer container = new RequestContainer();
		RequestElement el = new RequestElement();
		RequestAttributes attr = new RequestAttributes();
		attr.setAttributeId("ns:c3isp:dpo-metadata");
		attr.setValue(mapper.writeValueAsString(jsonMap));
		
		RequestAttributes dsaIdAttr = new RequestAttributes();
		dsaIdAttr.setAttributeId("ns:c3isp:dsa-id");
		dsaIdAttr.setValue(DSA_ID);

		LinkedList<RequestAttributes> list = new LinkedList<RequestAttributes>();
		list.add(attr);
		el.setAttributes(list);
		container.setRequest(el);
		
		System.err.println("create metadata");
		System.err.println(mapper.writeValueAsString(container));
		System.err.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(container));
		
		MultiValueMap<String, Object> parts = new LinkedMultiValueMap<String, Object>();

		parts.add("input_metadata", container);
		parts.add("fileToSubmit", new FileSystemResource(new File(fileURL.getFile())) );
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Basic " + base64Creds);

		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		HttpEntity<MultiValueMap<String, Object>> request = 
				new HttpEntity<MultiValueMap<String, Object>>(parts, headers);
		
		System.err.println(headers.getContentType());
		
//		TestRestTemplate restTemplate = new TestRestTemplate(httpClientOptions)
//		TestRestTemplate restTemplate = new TestRestTemplate(createSSLIgnoringRestTemplate());

		RestTemplate restTemplate = createSSLIgnoringRestTemplate();		

		ResponseEntity<RestResponse> response = null;
		
		for (int i = 0; i < 1; i++) {

			response = restTemplate//.withBasicAuth("user", "password")
					.exchange(isiApiURL+publishApi, HttpMethod.POST, request, RestResponse.class);

			RestResponse respBody = response.getBody();

			String dposId = respBody.getAdditionalProperties().get("dposId");

			System.err.println("dpo_id is: " + dposId);

			dpos.add(dposId);

		}
//		
/////////////////////////////////////////
//		call prepareData in ISI API
/////////////////////////////////////////

		URI address = UriComponentsBuilder.fromUriString(isiApiURL)
				.path(prepareDataAPI)
				.queryParam("data_lake", "VDL")
				.queryParam("format", "CEF") // ignored so far
				.queryParam("serviceName", "test_prepareData")
				.queryParam("type", "FS")
				.build()
				.toUri();		

		
				
		LinkedList<String> dpoList = new LinkedList<String>(); 
				
//		dpoList.add("\""+dposId+"\"");
//		dpoList.add("\""+dposId2+"\"");

		for (String dpoId : dpos) {
			dpoList.add(dpoId);
		}
		
		
		/**
		 * TODO check here, adding Mirko's DPO
		 */
		
//		dpoList.add(testDPO_ID_Obligations);
		
//		MultiValueMap<String, Object> parts1 = new LinkedMultiValueMap<String, Object>();

//		parts1.add("DPO_id_list", dpoList);
		
		MultiValueMap<String, String> headers1 = new LinkedMultiValueMap<String, String>();
		headers1.add("Authorization", "Basic " + base64Creds);
		headers1.add("Content-Type", "application/json");
		headers1.add("X-c3isp-input_metadata", createISIAPIjson(mapper));
		headers1.add("X-c3isp-metadata", createISIAPIjson(mapper));
		
		HttpEntity<LinkedList<String>> requestSpec = new HttpEntity<LinkedList<String>>(dpoList, headers1);

		
		
//		HttpEntity<MultiValueMap<String, Object>> request1 = 
//				new HttpEntity<MultiValueMap<String, Object>>(parts1, headers1);
		
		System.err.println("size of messageconverters before: "+restTemplate.getMessageConverters().size());
		
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

		System.err.println("size of messageconverters after: "+restTemplate.getMessageConverters().size());
		
		
		System.err.println("requestSpec is: "+requestSpec.toString());
		
		System.err.println("call isi api preparedata");
		
		
//		ResponseEntity<String> readResponse = restTemplate//.withBasicAuth("user", "password")
//		.exchange(
////				isiApiURL+publishApi+"/"+dposId+"/",
//				address,
//				HttpMethod.POST, 
//				request1, 
//				String.class);

		String readResponse = restTemplate
				.postForObject(address, requestSpec, String.class);
				
				
//		System.err.println("buffer manager response: status="+readResponse.getStatusCodeValue());
//		System.err.println("buffer manager response: value="+readResponse.getBody());
	
//		System.err.println("buffer manager response: status="+readResponse.getStatusCodeValue());
		System.err.println("isi api prepareData response: value="+readResponse);
		

		
/// DELETE
		
		MultiValueMap<String, Object> partsDelete = new LinkedMultiValueMap<String, Object>();
		
	
		
		HttpHeaders headersDelete = new HttpHeaders();
		headersDelete.add("Authorization", "Basic " + base64Creds);
		headersDelete.add("X-c3isp-input_metadata", "{}");
			
		headersDelete.setContentType(MediaType.APPLICATION_JSON_UTF8);
		HttpEntity<MultiValueMap<String, Object>> requestDelete = 
				new HttpEntity<MultiValueMap<String, Object>>(partsDelete, headersDelete);

		
		
		for (String dposId : dpos) {
		
			
			ResponseEntity<String> deleteResponse = restTemplate//.withBasicAuth("user", "password")
					.exchange(
							isiApiURL+publishApi+"/"+dposId+"/", 
							HttpMethod.DELETE, 
							requestDelete, 
							String.class);

			System.err.println("dposId: "+dposId+" : "+deleteResponse.getBody());
		}
		
//		System.err.println(deleteResponse.getBody());
	}
	

@Test	
	public void testAnonymizeIPv4toLocations() throws IOException {

	Vector<String> dpos = new Vector<String>();	
		
		
///////////////////////////////////////
// CREATE
///////////////////////////////////////

		
		String publishApi = "/v1/dpo";
		
		URL fileURL = this.getClass().getClassLoader().getResource(INPUT_FILE);

		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> jsonMap = mapper.readValue(testMetadata,
		    new TypeReference<Map<String,Object>>(){});
		
		jsonMap.put("dsa_id", DSA_ID_AnonymizeIpV4ToLocation);

		
		String plainCreds = "user:password";
		byte[] plainCredsBytes = plainCreds.getBytes();
		byte[] base64CredsBytes = Base64.getEncoder().encode(plainCredsBytes);
		String base64Creds = new String(base64CredsBytes);

		RequestContainer container = new RequestContainer();
		RequestElement el = new RequestElement();
		RequestAttributes attr = new RequestAttributes();
		attr.setAttributeId("ns:c3isp:dpo-metadata");
		attr.setValue(mapper.writeValueAsString(jsonMap));
		
		RequestAttributes dsaIdAttr = new RequestAttributes();
		dsaIdAttr.setAttributeId("ns:c3isp:dsa-id");
		dsaIdAttr.setValue(DSA_ID_AnonymizeIpV4ToLocation);

		LinkedList<RequestAttributes> list = new LinkedList<RequestAttributes>();
		list.add(attr);
		el.setAttributes(list);
		container.setRequest(el);
		
		System.err.println("create metadata");
		System.err.println(mapper.writeValueAsString(container));
		System.err.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(container));
		
		MultiValueMap<String, Object> parts = new LinkedMultiValueMap<String, Object>();

		parts.add("input_metadata", container);
		parts.add("fileToSubmit", new FileSystemResource(new File(fileURL.getFile())) );
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Basic " + base64Creds);

		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		HttpEntity<MultiValueMap<String, Object>> request = 
				new HttpEntity<MultiValueMap<String, Object>>(parts, headers);
		
		System.err.println(headers.getContentType());
		
//		TestRestTemplate restTemplate = new TestRestTemplate(httpClientOptions)
//		TestRestTemplate restTemplate = new TestRestTemplate(createSSLIgnoringRestTemplate());

		RestTemplate restTemplate = createSSLIgnoringRestTemplate();		

		ResponseEntity<RestResponse> response = null;
		
		for (int i = 0; i < 1; i++) {

			response = restTemplate//.withBasicAuth("user", "password")
					.exchange(isiApiURL+publishApi, HttpMethod.POST, request, RestResponse.class);

			RestResponse respBody = response.getBody();

			String dposId = respBody.getAdditionalProperties().get("dposId");

			System.err.println("dpo_id is: " + dposId);

			dpos.add(dposId);

		}
//		
/////////////////////////////////////////
//		call prepareData in ISI API
/////////////////////////////////////////

		URI address = UriComponentsBuilder.fromUriString(isiApiURL)
				.path(prepareDataAPI)
				.queryParam("data_lake", "VDL")
				.queryParam("format", "CEF") // ignored so far
				.queryParam("serviceName", "test_prepareData")
				.queryParam("type", "FS")
				.build()
				.toUri();		

		
				
		LinkedList<String> dpoList = new LinkedList<String>(); 
				
//		dpoList.add("\""+dposId+"\"");
//		dpoList.add("\""+dposId2+"\"");

		for (String dpoId : dpos) {
			dpoList.add(dpoId);
		}
		
		
		/**
		 * TODO check here, adding Mirko's DPO
		 */
		
//		dpoList.add(testDPO_ID_Obligations);
		
//		MultiValueMap<String, Object> parts1 = new LinkedMultiValueMap<String, Object>();

//		parts1.add("DPO_id_list", dpoList);
		
		MultiValueMap<String, String> headers1 = new LinkedMultiValueMap<String, String>();
		headers1.add("Authorization", "Basic " + base64Creds);
		headers1.add("Content-Type", "application/json");
		headers1.add("X-c3isp-input_metadata", createISIAPIjson(mapper));
		headers1.add("X-c3isp-metadata", createISIAPIjson(mapper));
		
		HttpEntity<LinkedList<String>> requestSpec = new HttpEntity<LinkedList<String>>(dpoList, headers1);

		
		
//		HttpEntity<MultiValueMap<String, Object>> request1 = 
//				new HttpEntity<MultiValueMap<String, Object>>(parts1, headers1);
		
		System.err.println("size of messageconverters before: "+restTemplate.getMessageConverters().size());
		
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

		System.err.println("size of messageconverters after: "+restTemplate.getMessageConverters().size());
		
		
		System.err.println("requestSpec is: "+requestSpec.toString());
		
		System.err.println("call isi api preparedata");
		
		
//		ResponseEntity<String> readResponse = restTemplate//.withBasicAuth("user", "password")
//		.exchange(
////				isiApiURL+publishApi+"/"+dposId+"/",
//				address,
//				HttpMethod.POST, 
//				request1, 
//				String.class);

		String readResponse = restTemplate
				.postForObject(address, requestSpec, String.class);
				
				
//		System.err.println("buffer manager response: status="+readResponse.getStatusCodeValue());
//		System.err.println("buffer manager response: value="+readResponse.getBody());
	
//		System.err.println("buffer manager response: status="+readResponse.getStatusCodeValue());
		System.err.println("isi api prepareData response: value="+readResponse);
		

		
/// DELETE
		
		MultiValueMap<String, Object> partsDelete = new LinkedMultiValueMap<String, Object>();
		
	
		
		HttpHeaders headersDelete = new HttpHeaders();
		headersDelete.add("Authorization", "Basic " + base64Creds);
		headersDelete.add("X-c3isp-input_metadata", "{}");
			
		headersDelete.setContentType(MediaType.APPLICATION_JSON_UTF8);
		HttpEntity<MultiValueMap<String, Object>> requestDelete = 
				new HttpEntity<MultiValueMap<String, Object>>(partsDelete, headersDelete);

		
		
		for (String dposId : dpos) {
		
			
			ResponseEntity<String> deleteResponse = restTemplate//.withBasicAuth("user", "password")
					.exchange(
							isiApiURL+publishApi+"/"+dposId+"/", 
							HttpMethod.DELETE, 
							requestDelete, 
							String.class);

			System.err.println("dposId: "+dposId+" : "+deleteResponse.getBody());
		}
		
//		System.err.println(deleteResponse.getBody());
	}


@Test	
	public void testAnonymizationIpV4RandomizationFromNetflow() throws IOException {

	Vector<String> dpos = new Vector<String>();	
		
		
///////////////////////////////////////
// CREATE
///////////////////////////////////////

		
		String publishApi = "/v1/dpo";
		
		URL fileURL = this.getClass().getClassLoader().getResource(INPUT_FILE_NETFLOW);

		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> jsonMap = mapper.readValue(testMetadata,
		    new TypeReference<Map<String,Object>>(){});
		
		String plainCreds = "user:password";
		byte[] plainCredsBytes = plainCreds.getBytes();
		byte[] base64CredsBytes = Base64.getEncoder().encode(plainCredsBytes);
		String base64Creds = new String(base64CredsBytes);

		RequestContainer container = new RequestContainer();
		RequestElement el = new RequestElement();
		RequestAttributes attr = new RequestAttributes();
		attr.setAttributeId("ns:c3isp:dpo-metadata");
		attr.setValue(mapper.writeValueAsString(jsonMap));
		
		RequestAttributes dsaIdAttr = new RequestAttributes();
		dsaIdAttr.setAttributeId("ns:c3isp:dsa-id");
		dsaIdAttr.setValue(DSA_ID);

		LinkedList<RequestAttributes> list = new LinkedList<RequestAttributes>();
		list.add(attr);
		el.setAttributes(list);
		container.setRequest(el);
		
		System.err.println("create metadata");
		System.err.println(mapper.writeValueAsString(container));
		System.err.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(container));
		
		MultiValueMap<String, Object> parts = new LinkedMultiValueMap<String, Object>();

		parts.add("input_metadata", container);
		parts.add("fileToSubmit", new FileSystemResource(new File(fileURL.getFile())) );
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Basic " + base64Creds);

		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		HttpEntity<MultiValueMap<String, Object>> request = 
				new HttpEntity<MultiValueMap<String, Object>>(parts, headers);
		
		System.err.println(headers.getContentType());
		
//		TestRestTemplate restTemplate = new TestRestTemplate(httpClientOptions)
//		TestRestTemplate restTemplate = new TestRestTemplate(createSSLIgnoringRestTemplate());

		RestTemplate restTemplate = createSSLIgnoringRestTemplate();		

		ResponseEntity<RestResponse> response = null;
		
		URI createAddresswithNorm = UriComponentsBuilder.fromUriString(isiApiURL)
				.path(publishApi)
				.queryParam("norm", "true")
				.build()
				.toUri();		

		
		for (int i = 0; i < 1; i++) {

			response = restTemplate//.withBasicAuth("user", "password")
					.exchange(
							createAddresswithNorm,
							HttpMethod.POST,
							request, 
							RestResponse.class
					);

			RestResponse respBody = response.getBody();

			String dposId = respBody.getAdditionalProperties().get("dposId");

			System.err.println("dpo_id is: " + dposId);

			dpos.add(dposId);

		}
//		
/////////////////////////////////////////
//		call prepareData in ISI API
/////////////////////////////////////////

		URI address = UriComponentsBuilder.fromUriString(isiApiURL)
				.path(prepareDataAPI)
				.queryParam("data_lake", "VDL")
				.queryParam("format", "CEF") // ignored so far
				.queryParam("serviceName", "test_prepareData")
				.queryParam("type", "FS")
				.build()
				.toUri();		

		
				
		LinkedList<String> dpoList = new LinkedList<String>(); 
				
//		dpoList.add("\""+dposId+"\"");
//		dpoList.add("\""+dposId2+"\"");

		for (String dpoId : dpos) {
			dpoList.add(dpoId);
		}
		
		
		/**
		 * TODO check here, adding Mirko's DPO
		 */
		
//		dpoList.add(testDPO_ID_Obligations);
		
//		MultiValueMap<String, Object> parts1 = new LinkedMultiValueMap<String, Object>();

//		parts1.add("DPO_id_list", dpoList);
		
		MultiValueMap<String, String> headers1 = new LinkedMultiValueMap<String, String>();
		headers1.add("Authorization", "Basic " + base64Creds);
		headers1.add("Content-Type", "application/json");
		headers1.add("X-c3isp-input_metadata", createISIAPIjson(mapper));
		headers1.add("X-c3isp-metadata", createISIAPIjson(mapper));
		
		HttpEntity<LinkedList<String>> requestSpec = new HttpEntity<LinkedList<String>>(dpoList, headers1);

		
		
//		HttpEntity<MultiValueMap<String, Object>> request1 = 
//				new HttpEntity<MultiValueMap<String, Object>>(parts1, headers1);
		
		System.err.println("size of messageconverters before: "+restTemplate.getMessageConverters().size());
		
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

		System.err.println("size of messageconverters after: "+restTemplate.getMessageConverters().size());
		
		
		System.err.println("requestSpec is: "+requestSpec.toString());
		
		System.err.println("call isi api preparedata");
		
		
//		ResponseEntity<String> readResponse = restTemplate//.withBasicAuth("user", "password")
//		.exchange(
////				isiApiURL+publishApi+"/"+dposId+"/",
//				address,
//				HttpMethod.POST, 
//				request1, 
//				String.class);

		String readResponse = restTemplate
				.postForObject(address, requestSpec, String.class);
				
				
//		System.err.println("buffer manager response: status="+readResponse.getStatusCodeValue());
//		System.err.println("buffer manager response: value="+readResponse.getBody());
	
//		System.err.println("buffer manager response: status="+readResponse.getStatusCodeValue());
		System.err.println("isi api prepareData response: value="+readResponse);
		

		
/// DELETE
		
		MultiValueMap<String, Object> partsDelete = new LinkedMultiValueMap<String, Object>();
		
	
		
		HttpHeaders headersDelete = new HttpHeaders();
		headersDelete.add("Authorization", "Basic " + base64Creds);
		headersDelete.add("X-c3isp-input_metadata", "{}");
			
		headersDelete.setContentType(MediaType.APPLICATION_JSON_UTF8);
		HttpEntity<MultiValueMap<String, Object>> requestDelete = 
				new HttpEntity<MultiValueMap<String, Object>>(partsDelete, headersDelete);

		
		
		for (String dposId : dpos) {
		
			
			ResponseEntity<String> deleteResponse = restTemplate//.withBasicAuth("user", "password")
					.exchange(
							isiApiURL+publishApi+"/"+dposId+"/", 
							HttpMethod.DELETE, 
							requestDelete, 
							String.class);

			System.err.println("dposId: "+dposId+" : "+deleteResponse.getBody());
		}
		
//		System.err.println(deleteResponse.getBody());
	}


@ApiModel(value="RestResponse",description="Required metadata to complete the operation")
static public class RestResponse {

    /**
     * (Required)
     * 
     */
	
    private String sessionId;

//	@ApiModelProperty(required=true, value="additionalProperties to be used for the operation", allowEmptyValue=true)
    private Map<String, String> additionalProperties;


	@ApiModelProperty(required=false, value = "session_ID to be used for the operation", allowEmptyValue=false)
	public String getSessionId() {
        return sessionId;
    }

    
    public void setSessionId(String dsaID) {
        this.sessionId = dsaID;
    }

    public RestResponse withSessionId(String sessionId) {
        this.sessionId = sessionId;
        return this;
    }
  
//    @XmlElementWrapper(name = "additionalProperties")
//    @XmlElement(name = "property")
    @ApiModelProperty(value = "an array of arbitrary elements ")
    public Map<String, String> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperties (Map<String, String> additonalProperties) {
    	this.additionalProperties = additonalProperties;
    }
    
    @Override
    public String toString() {
    	
    	StringWriter toReturn =  new StringWriter();
    	ObjectMapper mapper = new ObjectMapper();
    	try {
			mapper.writeValue(toReturn, this);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
    	return toReturn.toString();
    	
    }
 
};


@ApiModel(value = "RequestContainer", description = "Mapping request attributes in a XACML-like fashon")
// @XmlRootElement(name ="Request")
static public class RequestContainer {

//	@XmlElement(name = "Request", nillable = false, required = true)
	@JsonProperty("Request")
	RequestElement request;

	@ApiModelProperty(allowEmptyValue=false,required=true,name="Request")
	public RequestElement getRequest() {
		return request;
	}

	public void setRequest(RequestElement requestElement) {
		this.request = requestElement;
	}
	
	public RequestContainer() {
	
		request = new RequestElement();
		
	}
	
	/**
	 * serches for an attributeValue in a {@link RequestContainer}
	 * 
	 * @param attributeName
	 * @return a String with the attributeValue or <code>null</code> otherwise
	 */
	public String search(String attributeName) {
		if(request == null) {
			return null;
		}
		if (request.getAttributes().size() < 1) {
			return null;
		}
		for(RequestAttributes attr : request.getAttributes()) {
			if (attr.getAttributeId().compareToIgnoreCase(attributeName)==0) {
				return attr.getValue();
			}
		}
		return null;
	}
	
	@Override
	public String toString() {
		StringWriter toReturn =  new StringWriter();
    	ObjectMapper mapper = new ObjectMapper();
    	try {
			mapper.writeValue(toReturn, this);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
    	return toReturn.toString();
	}

};


@ApiModel(value="RequestAttributes",description="Container for attributes in a XACML-like fashon")
//@XmlRootElement(name ="Request")
public class RequestAttributes {

	@XmlElement(name = "AttributeId", nillable = false, required = true)
	@JsonProperty(value= "AttributeId",required = true)
	String attributeId;
	@XmlElement(name = "Value", nillable = false, required = true)
	@JsonProperty(value="Value",required=true)
	String value;
	@XmlElement(name = "DataType", nillable = false, required = true)
	@JsonProperty(value="DataType",required=true)
	String datatype;
	
	
	public RequestAttributes() {
		attributeId = "";
		value = "";
		datatype = "string";
	
	}
	
	public String getAttributeId() {
		return attributeId;
	}
	public void setAttributeId(String attributeId) {
		this.attributeId = attributeId;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getDatatype() {
		return datatype;
	}
	public void setDatatype(String datatype) {
		this.datatype = datatype;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof RequestAttributes) {
			RequestAttributes input = (RequestAttributes) obj;
			
			return 	input.attributeId.equals(attributeId)
					&& input.value.equals(value)
					&& input.datatype.equals(datatype);
			
		} else {
			return super.equals(obj);
		}
	}
};


@ApiModel(value = "RequestElement", description = "Holds the request elements")
@XmlRootElement(name = "Request")
static public class RequestElement {

	// @XmlElement(name = "AccessSubject", nillable = false, required = true)
	// BaseCategory accesssubject;
	// @XmlElement(name = "Action", nillable = false, required = true)
	// @ApiModelProperty(allowEmptyValue = false, required = true, position = 2)
	// Action action;
	// @JsonProperty("Action")
	// BaseCategory action;
	// @ApiModelProperty(allowEmptyValue = false, required = true, position = 3)
	// Resource resource;
	// @XmlElement(name = "Resource", nillable = false, required = true)
	// @JsonProperty("Resource")
	// BaseCategory resource;
	// @XmlElement(name = "Environment", nillable = false, required = true)
	// @ApiModelProperty(allowEmptyValue = false, required = true, position = 4)
	// @JsonProperty("Environment")
	// Environment environment;
	// BaseCategory environment;

	@ApiModelProperty(allowEmptyValue = true, required = true, value = "Attribute")
	List<RequestAttributes> attribute;

	
	@JsonProperty("Attribute")
	public List<RequestAttributes> getAttributes() {
		return attribute;
	}
	
	public void setAttributes(List<RequestAttributes> attributes) {
		this.attribute = attributes;
	}
	
	public RequestElement() {
	}


};
	
	
}
