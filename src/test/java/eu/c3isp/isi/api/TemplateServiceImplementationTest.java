/**
 *  Copyright 2017 Hewlett Packard Enterprise Development Company, L.P.
 */
package eu.c3isp.isi.api;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.cnr.iit.isi.api.restapi.types.xacml.RequestAttributes;
import it.cnr.iit.isi.api.restapi.types.xacml.RequestContainer;
import it.cnr.iit.isi.api.restapi.types.xacml.RequestElement;

// for code completion add MockMvcRequestBuilders and MockMvcRequestBuilders as 'favorite types' in eclipse
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*; //get, post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.net.URLEncoder;
import java.util.LinkedList;
import java.util.Map;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertTrue;

/**
 * @author MIMANE
 *
 */
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)

@ActiveProfiles("test") // load application-test.properties
@AutoConfigureMockMvc
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TemplateServiceImplementationTest {

    @Autowired
    private MockMvc mockMvc;
    
    @Autowired
    private ObjectMapper mapper;
    
    @Value("${security.user.name}")
    private String restUser;
    @Value("${security.user.password}")
    private String restPassword;
    
    
    
    
	final static String exempleRestMetadata = "{\"Request\":{\"AccessSubject\":{\"Attribute\":[{{\"AttributeId\":\"ns:c3isp:something\",\"Value\":\"somevalue\",\"DataType\":\"string\"}]},\"Action\":{},\"Resource\":{},\"Environment\":{\"Attribute\":[{\"AttributeId\":\"TestAttributeEnvironment\",\"Value\":\"testValue\",\"DataType\":\"string\"}]}}}";

	final static String testMetadataMirko = "{\"Request\":{\"Attribute\":[{\"AttributeId\":\"ns:c3isp:dpo-metadata\",\"Value\":\"{\\\"id\\\":\\\"4000123\\\",\\\"dsa_id\\\":\\\"DSA-c7aa89e7-ed98-42a8-91cf-74876c7ebc3e\\\",\\\"start_time\\\":\\\"2017-12-14T12:00:00.0Z\\\",\\\"end_time\\\":\\\"2017-12-14T18:01:01.0Z\\\",\\\"event_type\\\":\\\"Firewall Event\\\",\\\"organization\\\":\\\"3DRepo\\\"}\",\"DataType\":\"string\"}]}}";

	final static String dsaId  = "DSA-66749812-67b8-4e47-beb0-55fd99b835be";

	final static String sniffedPOSTpayload = "-----------------------------1640378158559742224736604238\r\n" + 
			"Content-Disposition: form-data; name=\"input_metadata\"\r\n" + 
			"\r\n" + 
			"   {          \"Request\": {              \"Attribute\": [{                  \"AttributeId\": \"ns:c3isp:dsa-id\",                  \"Value\": \"somevalue\",                  \"DataType\": \"string\"              },               {                 \"AttributeId\": \"ns:c3isp:dpo-metadata\",                  \"Value\": \"<DPOS METADATA STRING>\",                  \"DataType\": \"string\"              }         ]}     }\r\n" + 
			"-----------------------------1640378158559742224736604238\r\n" + 
			"Content-Disposition: form-data; name=\"fileToSubmit\"; filename=\"make.bat\"\r\n" + 
			"Content-Type: application/octet-stream\r\n" + 
			"\r\n" + 
			"@echo off\r\n" + 
			"\r\n" + 
			"raml2html service.raml > RAML.html && jsdoc -d jsdoc -r ../controller ../model/Prototypes.js ../util ../view\r\n" + 
			"-----------------------------1640378158559742224736604238--\r\n" + 
			"";
	

	@Test
	public void fakeTest() {
		assertTrue(true);
	}
	
//	@Test
    public void test01get() throws Exception {
        
        String param = "test";
        String expectedOutput = "aName";
        
        System.out.println(">>>>>>>>>>>"+restUser);
        this.mockMvc.perform(
                get("/v1/dpo/" + param + "/")
                    .with(httpBasic(restUser, restPassword)) // basic auth
                    .header("X-c3isp-input_metadata", exempleRestMetadata)
                    .accept(MediaType.APPLICATION_JSON)
                    .contentType(MediaType.APPLICATION_JSON)
                 )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
//                .andExpect(content().string(containsString(expectedOutput))) //check if the returned output (json) contains 'expectedOutput
                ;
    }
	
//	@Test
    public void test02get() throws Exception {
        
        String param = "test";
        //expected output structure = "{\"name\":\"aName\",\"id\":\"anIdValue\",\"path\":\"aPath\",\"version\":\"1\"}";
        
        this.mockMvc.perform(
                get("/v1/dpo/" + param + "/")
                    .with(httpBasic(restUser, restPassword)) // basic auth
                    .header("X-myheader-test1", "TEST1")
                    .header("X-myheader-test2", "TEST2")
                    .accept(MediaType.APPLICATION_JSON)
                    .contentType(MediaType.APPLICATION_JSON)
                 )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.name", is("aName")))
                ;
        
    }
    
//    @Test
    public void testCreate() throws Exception {
        
        String param = "test";
        //expected output structure = "{\"name\":\"aName\",\"id\":\"anIdValue\",\"path\":\"aPath\",\"version\":\"1\"}";
        
//        String fileName = "Trend Micro_Deep Security Manager_9.6_20_MSS FirewallEvent_3_.txt";
        String fileName =  "response.txt";

        
        
		URL fileURL = this.getClass().getClassLoader().getResource(fileName);

		Map<String, Object> jsonMap = mapper.readValue(testMetadataMirko,
			    new TypeReference<Map<String,Object>>(){});
		
		RequestContainer container = new RequestContainer();
		RequestElement el = new RequestElement();
		
		RequestAttributes attr = new RequestAttributes();
		attr.setAttributeId("ns:c3isp:dpo-metadata");
		attr.setValue(mapper.writeValueAsString(jsonMap));
		
		RequestAttributes dsaIdAttr = new RequestAttributes();
		dsaIdAttr.setAttributeId("ns:c3isp:dsa-id");
		dsaIdAttr.setValue(dsaId);

		LinkedList<RequestAttributes> list = new LinkedList<RequestAttributes>();
		
		list.add(attr);
		list.add(dsaIdAttr);
		
		el.setAttributes(list);
		container.setRequest(el);
		
		
        
        MockMultipartFile mockFile = new MockMultipartFile(
        		"fileToSubmit", 
        		fileName,
        		null,
        		new FileInputStream(fileURL.getFile()));
        
//        MockR
//        
          // doesn't work
//        MockHttpServletRequestBuilder builder =
//                MockMvcRequestBuilders.fileUpload("/v1/dpo/")
//                                      .file(mockFile)
//                                      .param("input_metadata", mapper.writeValueAsString(container))
//                                      .content(
//                                    		  buildUrlEncodedFormEntity(
//                                    				  "input_metadata",
//                                    				  mapper.writeValueAsString(container)))
//                                      .with(httpBasic(restUser, restPassword)) // basic auth
////                                      .header("X-myheader-test1", "TEST1")
//                                      .accept(MediaType.APPLICATION_JSON)
//                                      .contentType(MediaType.MULTIPART_FORM_DATA);


        
        MockHttpServletRequestBuilder builder =
        		MockMvcRequestBuilders.post("/v1/dpo/")
        			.with(httpBasic(restUser, restPassword)) // basic auth
                    .accept(MediaType.APPLICATION_JSON)
                    .contentType(MediaType.MULTIPART_FORM_DATA)
                    .requestAttr("input_metadata", mapper.writeValueAsString(container))
                    .requestAttr("fileToSubmit", mockFile)
        			.param("input_metadata", mapper.writeValueAsString(container))
        			.content(sniffedPOSTpayload)
        			;
        
        
        System.err.println("MockHttpServletRequestBuilder: "+builder.toString());
		
        MockMvcResultHandlers.print();
        
        this.mockMvc.perform(
        			builder
                 )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.name", is("aName")))
                ;
        
    }
    
    String buildUrlEncodedFormEntity(String... params) {
    	   if((params.length%2)>0) {
    	      throw new IllegalArgumentException("Need to give an even number of parameters");
    	   }
    	   StringBuilder result = new StringBuilder();
    	   
    	   for(int i=0; i<params.length; i+=2) {
    	   
    		   if(i>0) result.append('&');
    	      
    		   result.append(URLEncoder.encode(params[i]))
    				   .append('=')
    				   .append(URLEncoder.encode(params[i+1]));
    	   }
    	   
    	   return result.toString();
    	}
    
}
