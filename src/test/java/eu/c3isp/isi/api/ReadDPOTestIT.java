package eu.c3isp.isi.api;

import java.net.URI;
import java.util.Base64;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import it.cnr.iit.isi.api.restapi.types.xacml.RequestContainer;


@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)

@ActiveProfiles("test") // load application-test.properties
@AutoConfigureMockMvc
@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class ReadDPOTestIT {
	
	@Value("${isi.api.url}")
	String isiApiURL;

	@Test
	public void testReadDPO() {

		String readDPOApi = "/v1/dpo";
		
		String dposId = //"1542146844845-5f6c6019-2817-4ac5-a979-54affc89f42a";
				"1540886561505-79bfce79-1b7c-4bf3-8476";
		
		String plainCreds = "user:password";
		byte[] plainCredsBytes = plainCreds.getBytes();
		byte[] base64CredsBytes = Base64.getEncoder().encode(plainCredsBytes);
		String base64Creds = new String(base64CredsBytes);
		
		MultiValueMap<String, Object> parts = new LinkedMultiValueMap<String, Object>();
		

		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Basic " + base64Creds);
		headers.add("X-c3isp-input_metadata", new RequestContainer().toString());
		
		
//		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		HttpEntity<MultiValueMap<String, Object>> request = 
				new HttpEntity<MultiValueMap<String, Object>>(parts, headers);
		
		System.err.println(headers);
		
//		TestRestTemplate restTemplate = new TestRestTemplate(httpClientOptions)
//		TestRestTemplate restTemplate = new TestRestTemplate(createSSLIgnoringRestTemplate());

		RestTemplate restTemplate = TestUtils.createSSLIgnoringRestTemplate();

		URI readUri = UriComponentsBuilder.fromUriString(isiApiURL)
				.path(readDPOApi)
				.path("/"+ dposId +"/")
				.build()
				.toUri();
		
		ResponseEntity<String> response = restTemplate//.withBasicAuth("user", "password")
				.exchange(readUri, HttpMethod.GET, request, String.class);
		
		
//		System.err.println("cco_id is: " +result.getCco_id());
		System.err.println("dpo_id is: " +response.getBody());
		
		
	}
	
}
