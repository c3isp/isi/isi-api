package eu.c3isp.isi.api.restapi.types.xacml;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Iterator;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.cnr.iit.isi.api.restapi.impl.ApiCreateDPO;
import it.cnr.iit.isi.api.restapi.impl.ApiSearch;
import it.cnr.iit.isi.api.restapi.types.xacml.RequestContainer;
import it.cnr.iit.isi.api.restapi.types.xacml.StringtoXACMLRequestContainerConverter;

public class StringtoXACMLRequestContainerConverterTest {

	
	final String test1 = "{\"Request\":{\"Attribute\":[{\"AttributeId\":\"ns:c3isp:something\",\"Value\":\"somevalue\",\"DataType\":\"string\"}]}}";
	final String test2 = "{\"Request\":{\"Attribute\":[{\"AttributeId\":\"TestAttributeEnvironment\",\"Value\":\"testValue\",\"DataType\":\"string\"}, {\"AttributeId\":\"ns:c3isp:something\",\"Value\":\"somevalue\",\"DataType\":\"string\"}]}}";
	final String test3 = "{\"Request\":{\"Attribute\": []}}";
	
	public static final  String EXAMPLE_SEARCH_TEST = "{\"Request\": { \"Attribute\": [{\"AttributeId\": \"ns:c3isp:search-string\", \"Value\": \"_DPOS-SEARCH-STRING_\", \"DataType\": \"string\"}]}}";  
	
	@Test
	public void testInverse() throws JsonParseException, JsonMappingException, IOException {
		RequestContainer req = new RequestContainer();
		ObjectMapper mapper = new ObjectMapper();
		
//		System.err.println(req.toString());

		StringtoXACMLRequestContainerConverter converter = new StringtoXACMLRequestContainerConverter();
		
		assertNotNull(mapper.readValue(req.toString(), RequestContainer.class));
		
		RequestContainer retElement = mapper.readValue(EXAMPLE_SEARCH_TEST, RequestContainer.class);
		
//		System.err.println(retElement.toString());
		
		assertNotNull(converter.convert(req.toString()));
		
		JsonNode inputJson = mapper.readTree(EXAMPLE_SEARCH_TEST);
		
		System.err.println(inputJson.findValuesAsText("ns:c3isp:search-string"));

		assertTrue(retElement.search("ns:c3isp:search-string").compareTo("_DPOS-SEARCH-STRING_")==0);
		
		System.err.println(retElement.search("ns:c3isp:search-string"));
		
//		for (Iterator iter = inputJson.fieldNames()) {
//		
//			System.err.println();
//		}
		
	}
	
	@Test
	public void testConvert() {
		StringtoXACMLRequestContainerConverter converter = new StringtoXACMLRequestContainerConverter();
		
		assertNotNull(converter.convert(test3));
		assertNotNull(converter.convert(test2));
		assertNotNull(converter.convert(test1));
		
		assertNotNull(converter.convert(this.EXAMPLE_SEARCH_TEST));
		
		System.out.println(converter.convert(this.EXAMPLE_SEARCH_TEST));

	}

}
