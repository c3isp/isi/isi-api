package eu.c3isp.isi.api;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.Base64;
import java.util.LinkedList;
import java.util.Map;

import javax.net.ssl.SSLContext;

import org.apache.commons.io.FileUtils;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.tomcat.util.digester.DocumentProperties.Charset;
//import org.apache.commons.collections.map.MultiValueMap;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StreamUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.cnr.iit.isi.api.restapi.impl.ApiCreateDPO;
import it.cnr.iit.isi.api.restapi.types.RestResponse;
import it.cnr.iit.isi.api.restapi.types.xacml.RequestAttributes;
import it.cnr.iit.isi.api.restapi.types.xacml.RequestContainer;
import it.cnr.iit.isi.api.restapi.types.xacml.RequestElement;

@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)

@ActiveProfiles("test") // load application-test.properties
@AutoConfigureMockMvc
@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class MoveDPOTestIT {
	
	String dsaId  = //"DSA-66749812-67b8-4e47-beb0-55fd99b835be";
			
//			"DSA-c7aa89e7-ed98-42a8-91cf-74876c7ebc3e";
			"DSA-1180e376-f0d6-4c1c-bcd7-5aff4ac10895";
	
	
	String test = ApiCreateDPO.EXAMPLERESTMETADATA;
//	String testMetadata = "{            \"id\" : \"4000123\",       \"dsa_id\" :  \""+dsaId+"\",       \"start_time\" : \"2017-12-14T12:00:00.0Z\",       \"end_time\" : \"2017-12-14T18:01:01.0Z\",       \"event_type\" : \"Firewall Event\",       \"organization\" : \"3DRepo\" }";
	
//	String testOnlyDPOMetadataMirko = "{\"id\":\"4000123\",\"dsa_id\":\""+dsaId+"\",\"start_time\":\"2017-12-14T12:00:00.0Z\",\"end_time\":\"2017-12-14T18:01:01.0Z\",\"event_type\":\"Firewall Event\",\"organization\":\"3DRepo\"}";

//	String testMetadataMirko = "{\"Request\":{\"Attribute\":[{\"AttributeId\":\"ns:c3isp:dpo-metadata\",\"Value\":\"{\\\"id\\\":\\\"4000123\\\",\\\"dsa_id\\\":\\\"DSA-56976731-3c16-46cc-a4e1-8384c6208eb0\\\",\\\"start_time\\\":\\\"2017-12-14T12:00:00.0Z\\\",\\\"end_time\\\":\\\"2017-12-14T18:01:01.0Z\\\",\\\"event_type\\\":\\\"Firewall Event\\\",\\\"organization\\\":\\\"3DRepo\\\"}\",\"DataType\":\"string\"}]}}";
	// with DSA-id updated on 20181207
	String testMetadataMirko = "{\"Request\":{\"Attribute\":[{\"AttributeId\":\"ns:c3isp:dpo-metadata\",\"Value\":\"{\\\"id\\\":\\\"4000123\\\",\\\"dsa_id\\\":\\\""+dsaId+"\\\",\\\"start_time\\\":\\\"2017-12-14T12:00:00.0Z\\\",\\\"end_time\\\":\\\"2017-12-14T18:01:01.0Z\\\",\\\"event_type\\\":\\\"Firewall Event\\\",\\\"organization\\\":\\\"3DRepo\\\"}\",\"DataType\":\"string\"}]}}";
	
	
//	String testMetadataMirkoWrong = "{\"Request\":{\"Attribute\":[{\"AttributeId\":\"ns:c3isp:dpo-metadata\",\"Value\":{\"id\":\"4000123\",\"dsa_id\":\"DSA-56976731-3c16-46cc-a4e1-8384c6208eb0\",\"start_time\":\"2017-12-14T12:00:00.0Z\",\"end_time\":\"2017-12-14T18:01:01.0Z\",\"event_type\":\"Firewall Event\",\"organization\":\"3DRepo\"},\"DataType\":\"string\"}]}}";
	
//	String dsaId = "DSA-56976731-3c16-46cc-a4e1-8384c6208eb0";
	
	
	
	@Value("${isi.api.url}")
	String isiApiURL;
	
	@Autowired
	TestRestTemplate restTemplate;
	
	
//	@Test
	public void stupidTest() {
		URI transferAddress = UriComponentsBuilder.fromUriString("https://address")
				.path("dpo/transfer")
				.queryParam("norm", "false")
				.build()
				.toUri();
		
		System.err.println(transferAddress.toASCIIString());
	}
	
	@Test	
	public void testCreateMirkoNorm() throws IOException {

///////////////////////////////////////
// CREATE
///////////////////////////////////////

		
		String publishApi = "/v1/dpo";
		
//		URL fileURL = this.getClass().getClassLoader().getResource("response.txt");

//		URL fileURL = this.getClass().getClassLoader().getResource("Trend Micro_Deep Security Manager_9.6_20_MSS FirewallEvent_3_.txt");
		URL fileURL = this.getClass().getClassLoader().getResource("Trend_Micro_Deep_Security_Manager_9.6_20_MSS_FirewallEvent_3_.txt");
//		URL fileURL = this.getClass().getClassLoader().getResource("wenjun-test.txt");
		
		System.err.println("input file to be used is: "+fileURL.toString());
//		System.err.println("input file content:"+ StreamUtils.copyToString(fileURL.openStream(), java.nio.charset.Charset.forName("UTF-8")));
		
		
		ObjectMapper mapper = new ObjectMapper();
//		Map<String, Object> jsonMap = mapper.readValue(testMetadata,
//		    new TypeReference<Map<String,Object>>(){});
		
		String plainCreds = "user:password";
		byte[] plainCredsBytes = plainCreds.getBytes();
		byte[] base64CredsBytes = Base64.getEncoder().encode(plainCredsBytes);
		String base64Creds = new String(base64CredsBytes);

//		RequestContainer container = new RequestContainer();
//		RequestElement el = new RequestElement();
//		
//		RequestAttributes attr = new RequestAttributes();
//		attr.setAttributeId("ns:c3isp:dpo-metadata");
//		attr.setValue(mapper.writeValueAsString(jsonMap));
//		
//		RequestAttributes dsaIdAttr = new RequestAttributes();
//		dsaIdAttr.setAttributeId("ns:c3isp:dsa-id");
//		dsaIdAttr.setValue(dsaId);
//		
//		
//		LinkedList<RequestAttributes> list = new LinkedList<RequestAttributes>();
//		
//		
//		list.add(attr);
//		list.add(dsaIdAttr);
//		
//		el.setAttributes(list);
//		container.setRequest(el);
		
		System.err.println("create metadata");
		System.err.println(mapper.writeValueAsString(mapper.readTree(testMetadataMirko)));
		System.err.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(mapper.readTree(testMetadataMirko)));
		
		MultiValueMap<String, Object> parts = new LinkedMultiValueMap<String, Object>();

		parts.add("input_metadata", mapper.writeValueAsString(mapper.readTree(testMetadataMirko)));
		parts.add("fileToSubmit", new FileSystemResource(new File(fileURL.getFile())) );
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Basic " + base64Creds);
		

		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		HttpEntity<MultiValueMap<String, Object>> request = 
				new HttpEntity<MultiValueMap<String, Object>>(parts, headers);
		
		System.err.println(headers.getContentType());
		
		System.err.println(request);
		
//		TestRestTemplate restTemplate = new TestRestTemplate(httpClientOptions)
//		TestRestTemplate restTemplate = new TestRestTemplate(createSSLIgnoringRestTemplate());

		RestTemplate restTemplate = TestUtils.createSSLIgnoringRestTemplate();		
		
		URI address = UriComponentsBuilder.fromUriString(isiApiURL)
				.path(publishApi)
				.queryParam("norm", "true")
				.build()
				.toUri();
		
		ResponseEntity<RestResponse> response = restTemplate//.withBasicAuth("user", "password")
				.exchange(address, HttpMethod.POST, request, RestResponse.class);
		
		RestResponse respBody = response.getBody();
		
		String dposId = respBody.getAdditionalProperties().get("dposId");
		
		System.err.println("dpo_id is: " + dposId);
		
///////////////////////////////////////
		// READ
///////////////////////////////////////
		
		MultiValueMap<String, Object> parts1 = new LinkedMultiValueMap<String, Object>();

//		parts.add("input_metadata", container);
//		parts.add("fileToSubmit", new FileSystemResource(new File(fileURL.getFile())) );
		
		
		// input_metadata for read
		
		RequestContainer read_container = new RequestContainer();
		RequestElement el = new RequestElement();

		RequestAttributes attr = new RequestAttributes();
		attr.setAttributeId("urn:oasis:names:tc:xacml:3.0:subject:access-purpose");
		attr.setValue("generic_test_purpose");

		LinkedList<RequestAttributes> list = new LinkedList<RequestAttributes>();

		list.add(attr);

		el.setAttributes(list);
		read_container.setRequest(el);


		
		HttpHeaders headers1 = new HttpHeaders();
		headers1.add("Authorization", "Basic " + base64Creds);
//		headers1.add("X-c3isp-input_metadata", URLEncoder.encode("{}", "UTF-8"));
//		headers1.add("X-c3isp-input_metadata", "{}");
		headers1.add("X-c3isp-input_metadata", mapper.writeValueAsString(read_container));
		
				
		
			
//		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		HttpEntity<MultiValueMap<String, Object>> request1 = 
				new HttpEntity<MultiValueMap<String, Object>>(parts1, headers1);
		
		System.err.println(request1);
		
//		ResponseEntity<FileSystemResource> readResponse = restTemplate//.withBasicAuth("user", "password")
//				.exchange(
//						isiApiURL+publishApi+"/"+dposId+"/", 
//						HttpMethod.GET, 
//						request1, 
//						FileSystemResource.class);
		
		System.err.println("read metadata");
		
		
		ResponseEntity<ByteArrayResource> readResponse = restTemplate//.withBasicAuth("user", "password")
		.exchange(
				isiApiURL+publishApi+"/"+dposId+"/", 
				HttpMethod.GET, 
				request1, 
				ByteArrayResource.class);
		
		
		ByteArrayResource file = readResponse.getBody(); 
		File tmpFile = File.createTempFile("udd", ".fff");
		FileUtils.forceDeleteOnExit(tmpFile);
		FileUtils.copyInputStreamToFile(file.getInputStream(), tmpFile);
		
		String tmpFileContent = FileUtils.readFileToString(tmpFile);
		
		System.err.println(tmpFileContent);
		
//		assertTrue(FileUtils.contentEquals(tmpFile, new File(fileURL.getFile())));
		assertTrue(tmpFileContent.contains("stix"));
		
//		ResponseEntity<String> deleteResponse = 
//				restTemplate.delete(
//				isiApiURL+publishApi+"/"+dposId+"/");
		

////////////////////
/// MOVE
///////////////////
		
		System.err.println("before the move");

		String moveApi = "/v1/move/dpo";

		MultiValueMap<String, Object> partsMove = new LinkedMultiValueMap<String, Object>();

		//parts.add("input_metadata", container);
		//parts.add("fileToSubmit", new FileSystemResource(new File(fileURL.getFile())) );



		HttpHeaders headersMove = new HttpHeaders();
		headersMove.add("Authorization", "Basic " + base64Creds);
		//headers1.add("X-c3isp-input_metadata", URLEncoder.encode("{}", "UTF-8"));
		headersMove.add("X-c3isp-input_metadata", "{}");
		headersMove.add("X-c3isp-destination","https://isic3isp.iit.cnr.it:8443/isi-api/v1/");

//		headersMove.add("X-c3isp-destination","https://isic3isp.iit.cnr.it:8443/isi-api/api");
		

		//headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		HttpEntity<MultiValueMap<String, Object>> requestMove = 
				new HttpEntity<MultiValueMap<String, Object>>(partsMove, headersMove);

		System.err.println(request1);

		//ResponseEntity<FileSystemResource> readResponse = restTemplate//.withBasicAuth("user", "password")
		//.exchange(
		//	isiApiURL+publishApi+"/"+dposId+"/", 
		//	HttpMethod.GET, 
		//	request1, 
		//	FileSystemResource.class);

		System.err.println("read metadata");

		System.err.println("destination address: " 
				+ isiApiURL+moveApi+"/"+dposId+"/");

		ResponseEntity<String> moveResponse = restTemplate//.withBasicAuth("user", "password")
				.exchange(
						isiApiURL+moveApi+"/"+dposId+"/", 
						HttpMethod.POST, 
						requestMove, 
						String.class);


		String moveStringResponse = moveResponse.getBody();
		System.err.println("Move Response is: "+moveStringResponse);
		//File tmpFile = File.createTempFile("udd", ".fff");
		//FileUtils.forceDeleteOnExit(tmpFile);
		//FileUtils.copyInputStreamToFile(file.getInputStream(), tmpFile);
		//assertTrue(FileUtils.contentEquals(tmpFile, new File(fileURL.getFile())));
		//

		
		
		MultiValueMap<String, Object> parts2 = new LinkedMultiValueMap<String, Object>();

//		parts.add("input_metadata", container);
//		parts.add("fileToSubmit", new FileSystemResource(new File(fileURL.getFile())) );
		
		
		// input_metadata for read
		
		RequestContainer read_container2 = new RequestContainer();
		RequestElement el2 = new RequestElement();

		RequestAttributes attr2 = new RequestAttributes();
		attr.setAttributeId("urn:oasis:names:tc:xacml:3.0:subject:access-purpose");
		attr.setValue("generic_test_purpose");

		LinkedList<RequestAttributes> list2 = new LinkedList<RequestAttributes>();

		list2.add(attr2);

		el2.setAttributes(list2);
		read_container2.setRequest(el2);


		
		HttpHeaders headers2 = new HttpHeaders();
		headers2.add("Authorization", "Basic " + base64Creds);
//		headers1.add("X-c3isp-input_metadata", URLEncoder.encode("{}", "UTF-8"));
//		headers1.add("X-c3isp-input_metadata", "{}");
		headers2.add("X-c3isp-input_metadata", mapper.writeValueAsString(read_container2));
		
				
		
			
//		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		HttpEntity<MultiValueMap<String, Object>> request2 = 
				new HttpEntity<MultiValueMap<String, Object>>(parts2, headers2);
		
		System.err.println(request2);
		
//		ResponseEntity<FileSystemResource> readResponse = restTemplate//.withBasicAuth("user", "password")
//				.exchange(
//						isiApiURL+publishApi+"/"+dposId+"/", 
//						HttpMethod.GET, 
//						request1, 
//						FileSystemResource.class);
		
		System.err.println("read metadata");
		
		
		ResponseEntity<ByteArrayResource> readResponse2 = restTemplate//.withBasicAuth("user", "password")
		.exchange(
				isiApiURL+publishApi+"/"+dposId+"/", 
				HttpMethod.GET, 
				request2, 
				ByteArrayResource.class);
		
		
		ByteArrayResource file2 = readResponse.getBody(); 
		File tmpFile2 = File.createTempFile("udd", ".fff");
		FileUtils.forceDeleteOnExit(tmpFile2);
		FileUtils.copyInputStreamToFile(file2.getInputStream(), tmpFile2);
		
		String tmpFileContent2 = FileUtils.readFileToString(tmpFile2);
		
//		System.err.println(tmpFileContent);
		
		assertTrue(FileUtils.contentEquals(tmpFile2, new File(fileURL.getFile())));
//		assertTrue(tmpFileContent.contains("stix"));
		
//		ResponseEntity<String> deleteResponse = 
//				restTemplate.delete(
//				isiApiURL+publishApi+"/"+dposId+"/");
		
		
		
/// DELETE
		
		MultiValueMap<String, Object> partsDelete = new LinkedMultiValueMap<String, Object>();
		
	
		
		HttpHeaders headersDelete = new HttpHeaders();
		headersDelete.add("Authorization", "Basic " + base64Creds);
		headersDelete.add("X-c3isp-input_metadata", "{}");
			
		headersDelete.setContentType(MediaType.APPLICATION_JSON_UTF8);
		HttpEntity<MultiValueMap<String, Object>> requestDelete = 
				new HttpEntity<MultiValueMap<String, Object>>(partsDelete, headersDelete);

		
		
		ResponseEntity<String> deleteResponse = restTemplate//.withBasicAuth("user", "password")
				.exchange(
						isiApiURL+publishApi+"/"+dposId+"/", 
						HttpMethod.DELETE, 
						requestDelete, 
						String.class);
		
//		System.err.println(deleteResponse.getBody());
	}

	
	
}
