package eu.c3isp.isi.api;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.Base64;
import java.util.LinkedList;
import java.util.Map;
import java.util.Vector;

import javax.net.ssl.SSLContext;
import javax.print.DocFlavor.INPUT_STREAM;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.tomcat.util.digester.DocumentProperties.Charset;
//import org.apache.commons.collections.map.MultiValueMap;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.cnr.iit.isi.api.restapi.impl.ApiCreateDPO;
import it.cnr.iit.isi.api.restapi.types.RestResponse;
import it.cnr.iit.isi.api.restapi.types.xacml.RequestAttributes;
import it.cnr.iit.isi.api.restapi.types.xacml.RequestContainer;
import it.cnr.iit.isi.api.restapi.types.xacml.RequestElement;

@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)

@ActiveProfiles("test") // load application-test.properties
@AutoConfigureMockMvc
@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class formatAdapterTestIT {
	
	
	@Value("${format.adapter.endpoint}")
	String formatAdapterApiURL;
	
	@Autowired
	TestRestTemplate restTemplate;
	
	private File inputStreamToTempFile(InputStream inputStream) throws IOException {
		File tmpFile;
		tmpFile = File.createTempFile("publish-inputfile", "tmp");
		tmpFile.deleteOnExit();

		FileUtils.forceDeleteOnExit(tmpFile);
		FileUtils.copyInputStreamToFile(inputStream, tmpFile);
		return tmpFile;
	}	
	
	@Test	
	public void testFormat() throws IOException {

	URL fileURL = this.getClass().getClassLoader().getResource("2018-12-03T102102.000+0100.fgt");

	File inputFile = new File(fileURL.getFile());
	
	inputFile.getName().lastIndexOf('.');
	
	System.err.println("filename is: "+FilenameUtils.getBaseName(inputFile.getName()));
	System.err.println("file EXTENSION is: "+FilenameUtils.getExtension(inputFile.getName()));
	
	File ttt = File.createTempFile(FilenameUtils.getBaseName(inputFile.getName()), "."+FilenameUtils.getExtension(inputFile.getName()));
	ttt.deleteOnExit();
	FileUtils.forceDeleteOnExit(ttt);
	
	System.err.println("if we create a tmp file, it is in: "+ttt.getAbsolutePath()+" filename: "+ttt.getName());
	
	
	
	// to be changed
	File tmpFile = inputFile;
	
			
			
	System.err.println("server url is: "+formatAdapterApiURL);
	
	String formatApi = "/convert";

	String plainCreds = "user:password";
	byte[] plainCredsBytes = plainCreds.getBytes();
	byte[] base64CredsBytes = Base64.getEncoder().encode(plainCredsBytes);
	String base64Creds = new String(base64CredsBytes);

	
	MultiValueMap<String, Object> parts = new LinkedMultiValueMap<String, Object>();

	FileSystemResource fileSRes = new FileSystemResource(tmpFile);
	
//	fileSRes.set
	
	parts.add("file",  fileSRes);
	
	HttpHeaders headers = new HttpHeaders();
	headers.add("Authorization", "Basic " + base64Creds);

	headers.setContentType(MediaType.MULTIPART_FORM_DATA);
	HttpEntity<MultiValueMap<String, Object>> request = 
			new HttpEntity<MultiValueMap<String, Object>>(parts, headers);
	
	System.err.println(headers.getContentType());

	RestTemplate restTemplate = TestUtils.createSSLIgnoringRestTemplate();		
	
	ResponseEntity<ByteArrayResource> response = restTemplate//.withBasicAuth("user", "password")
			.exchange(formatAdapterApiURL+formatApi, HttpMethod.POST, request, ByteArrayResource.class);
	
	
	ByteArrayResource file = response.getBody(); 
	File tmpRFile = File.createTempFile("udd", ".fff");
	System.err.println("Tmp File is: "+tmpRFile.getAbsolutePath());
	FileUtils.forceDeleteOnExit(tmpRFile);
	FileUtils.copyInputStreamToFile(file.getInputStream(), tmpRFile);
	
	
	
	System.err.println("result is: \n"+FileUtils.readFileToString(tmpRFile));
	
	}

	@Test	
	public void testFormatWenjun() throws IOException {

//	URL fileURL = this.getClass().getClassLoader().getResource("wenjun-test.txt");
	URL fileURL = this.getClass().getClassLoader().getResource(
			"Trend Micro_Deep Security Manager_9.6_20_MSS FirewallEvent_3_.txt");

	
	
	System.err.println("server url is: "+formatAdapterApiURL);
	
	String formatApi = "/convert";

	String plainCreds = "user:password";
	byte[] plainCredsBytes = plainCreds.getBytes();
	byte[] base64CredsBytes = Base64.getEncoder().encode(plainCredsBytes);
	String base64Creds = new String(base64CredsBytes);

	
	MultiValueMap<String, Object> parts = new LinkedMultiValueMap<String, Object>();

	parts.add("file", new FileSystemResource(new File(fileURL.getFile())) );
	
	HttpHeaders headers = new HttpHeaders();
	headers.add("Authorization", "Basic " + base64Creds);

	headers.setContentType(MediaType.MULTIPART_FORM_DATA);
	HttpEntity<MultiValueMap<String, Object>> request = 
			new HttpEntity<MultiValueMap<String, Object>>(parts, headers);
	
	System.err.println(headers.getContentType());

	RestTemplate restTemplate = TestUtils.createSSLIgnoringRestTemplate();		
	
	ResponseEntity<ByteArrayResource> response = restTemplate//.withBasicAuth("user", "password")
			.exchange(formatAdapterApiURL+formatApi, HttpMethod.POST, request, ByteArrayResource.class);
	
	
	ByteArrayResource file = response.getBody(); 
	File tmpFile = File.createTempFile("udd", ".fff");
	FileUtils.forceDeleteOnExit(tmpFile);
	FileUtils.copyInputStreamToFile(file.getInputStream(), tmpFile);
	
	
	
	System.err.println("result is: \n"+FileUtils.readFileToString(tmpFile));
	
	}

	
	
	@Test	
	public void testDoubleSubmission() throws IOException {

	URL fileURL = this.getClass().getClassLoader().getResource("2018-12-03T102102.000+0100.fgt");

	
	String formatApi = "/convert";

	String plainCreds = "user:password";
	byte[] plainCredsBytes = plainCreds.getBytes();
	byte[] base64CredsBytes = Base64.getEncoder().encode(plainCredsBytes);
	String base64Creds = new String(base64CredsBytes);

	
	MultiValueMap<String, Object> parts = new LinkedMultiValueMap<String, Object>();

	parts.add("file", new FileSystemResource(new File(fileURL.getFile())) );
	
	HttpHeaders headers = new HttpHeaders();
	headers.add("Authorization", "Basic " + base64Creds);

	headers.setContentType(MediaType.MULTIPART_FORM_DATA);
	HttpEntity<MultiValueMap<String, Object>> request = 
			new HttpEntity<MultiValueMap<String, Object>>(parts, headers);
	
	System.err.println(headers.getContentType());

	RestTemplate restTemplate = TestUtils.createSSLIgnoringRestTemplate();		
	
	ResponseEntity<ByteArrayResource> response = restTemplate//.withBasicAuth("user", "password")
			.exchange(formatAdapterApiURL+formatApi, HttpMethod.POST, request, ByteArrayResource.class);
	
	
	ByteArrayResource file = response.getBody(); 
	File tmpFile = File.createTempFile("udd", ".fff");
	FileUtils.forceDeleteOnExit(tmpFile);
	FileUtils.copyInputStreamToFile(file.getInputStream(), tmpFile);
	
	
	
	System.err.println("result is: \n"+FileUtils.readFileToString(tmpFile));
	
	System.err.println("\n !!!! Resubmission !!!! \n");
	
	parts.remove("file");
	parts.add("file", new FileSystemResource(tmpFile) );
	
	ResponseEntity<ByteArrayResource> response2 = restTemplate//.withBasicAuth("user", "password")
			.exchange(formatAdapterApiURL+formatApi, HttpMethod.POST, request, ByteArrayResource.class);
	
	ByteArrayResource file2 = response2.getBody(); 
	File tmpFile2 = File.createTempFile("udd", ".fff");
	FileUtils.forceDeleteOnExit(tmpFile2);
	FileUtils.copyInputStreamToFile(file2.getInputStream(), tmpFile2);
	
	
	
	System.err.println("result is: \n"+FileUtils.readFileToString(tmpFile2));
	
	}
	
	

	
}
