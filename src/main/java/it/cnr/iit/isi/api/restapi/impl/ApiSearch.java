/**
 *  Copyright 2017 Hewlett Packard Enterprise Development Company, L.P.
 */
package it.cnr.iit.isi.api.restapi.impl;

import java.io.IOException;
import java.net.URI;
import java.util.Iterator;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import it.cnr.iit.isi.api.restapi.types.SearchParams;
import it.cnr.iit.isi.api.restapi.types.xacml.RequestAttributes;
import it.cnr.iit.isi.api.restapi.types.xacml.RequestContainer;

@ApiModel(value = "Template", description = "Template of REST APIs")
@RestController
@RequestMapping("/v1")
public class ApiSearch {

	/**
	 * These two are examples of configuration extracted from the
	 * application.properties file under the src/main/resources. Springboot will
	 * automatically load the file and its values, if file is not in the local path,
	 * it can be in the java classpath or set as an environment variable
	 */
	@Value("${security.user.name}")
	private String restUser;
	@Value("${security.user.password}")
	private String restPassword;

	@Value("${search.dpo.endpoint}")
	private String searchEndPoint;

	@Value("${search.dsa.endpoint}")
	private String searchDSAEndPoint;

	@Autowired
	private ObjectMapper objectMapper;

	public static final String EXAMPLE_SEARCH = "" + "		{ \r\n" + " 			\"Request\": { \r\n"
			+ "      	\t\"Attribute\": [" + "      	\t\t {\"AttributeId\": \"ns:c3isp:search-string\", \r\n"
			+ "      	\t\t  \"Value\": \"_DPOS-SEARCH-STRING_\", \r\n" + "       \t\t \"DataType\": \"string\" \r\n"
			+ "       \t\t},{...}\r\n" + "     \t]}\r\n" + "		} ";

	/**
	 * Used to call REST endpoints; configured by RestTemplateBuilder
	 */
	@Autowired
	private RestTemplate restTemplate;

	/**
	 * Logger class, can be used within the methods to log information
	 */
	private final static Logger LOGGER = LoggerFactory.getLogger(ApiSearch.class);

	@ApiOperation(httpMethod = "POST", value = "Search on DPO Store (dpos) or DSA Store (dsas)", tags = "ISI_API", notes = "Searches on the specified store, returns ... result data model TBD. Input format: \n\n\t"
			+ ApiSearch.EXAMPLE_SEARCH)
	@ApiResponses(value = { @ApiResponse(code = 412, message = "Client Error"),
			@ApiResponse(code = 500, message = "Internal server error"),
			@ApiResponse(code = 200, message = "Updated succesfully") })
	@RequestMapping(method = RequestMethod.POST, value = "/search/{store}/{longResultFlag}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> search(
			@PathVariable("store") @ApiParam(allowableValues = "dpos, dsas", required = true) @NotNull String store,
			@PathVariable("longResultFlag") @ApiParam(example = "it can only be true or false", required = true, allowMultiple = false) boolean longResultFlag,
			@RequestBody() @NotNull @ApiParam(example = ApiSearch.EXAMPLE_SEARCH) RequestContainer params) {

		String urlToUse = (store.compareTo("dpos") == 0) ? searchEndPoint : searchDSAEndPoint;

		URI url = UriComponentsBuilder.fromUriString(urlToUse).pathSegment(String.valueOf(longResultFlag)).build()
				.toUri();

		SearchParams searchParams = null;
		String searchParamsString = "";

		List<RequestAttributes> attrs = params.getRequest().getAttributes();

		for (Iterator iterator = attrs.iterator(); iterator.hasNext();) {
			RequestAttributes requestAttributes = (RequestAttributes) iterator.next();
			if (requestAttributes.getAttributeId().equals("ns:c3isp:search-string")) {
				try {
					searchParams = objectMapper.readValue(requestAttributes.getValue(), SearchParams.class);
					searchParamsString = requestAttributes.getValue();
				} catch (IOException e) {

					e.printStackTrace();

					return new ResponseEntity<String>(
							"{\"error\":\"malformed input, expected " + "RequestContainer with attributeId=\'ns:c3isp"
									+ ":search-string\' and value of type " + "SearchParams\"}",
							HttpStatus.BAD_REQUEST);

				}
			}
		}

		if (searchParams == null) {
			return new ResponseEntity<String>(
					"{\"error\":\"malformed input, expected " + "RequestContainer with attributeId=\'ns:c3isp"
							+ ":search-string\' and value of type " + "SearchParams\"}",
					HttpStatus.BAD_REQUEST);
		}

		// here, we have the correct SearchParameters
		ResponseEntity<String> response = restTemplate.postForEntity(url, searchParamsString, String.class);

		LOGGER.info("Received: " + longResultFlag + " " + searchParams.getCombiningRule());

		return new ResponseEntity<String>(response.getBody(), response.getStatusCode());
	}
}
