/**
 *  Copyright 2017 Hewlett Packard Enterprise Development Company, L.P.
 */
package it.cnr.iit.isi.api.restapi.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import it.cnr.iit.common.types.CreateBodyParams;
import it.cnr.iit.isi.api.restapi.types.RestResponse;
import it.cnr.iit.isi.utility.Utils;
import it.cnr.iit.vault.types.VaultDatakey;
import it.cnr.iit.vault.types.VaultResponse;
import it.cnr.iit.vault.types.VaultWrapper;

@ApiModel(value = "Template", description = "Template of REST APIs")
@RestController
@RequestMapping("/v1")
public class ApiCreateDPO {

	@Value("${rest.endpoint.url.callGet}")
	private String callGetEndpoint;
	@Value("${dsa.adapter.frontend.endpoint}")
	private String dsaAdapterFrontendURL;
	@Value("${format.adapter.endpoint}")
	private String formatAdapterURL;
	@Value("${security.user.name}")
	private String restUser;
	@Value("${security.user.password}")
	private String restPassword;

	@Autowired
	private RestTemplate restTemplate;
	@Autowired
	private ObjectMapper objectMapper;
	@Autowired
	private VaultWrapper vaultWrapper;
	@Autowired
	private Utils utils;

	final public static String EXAMPLERESTMETADATA = "" + "		{ \r\n" + "  			\"Request\": { \r\n"
			+ "      			\"Attribute\": [{ \r\n" + "        			\"AttributeId\": \"ns:c3isp:dsa-id\", \r\n"
			+ "        			\"Value\": \"somevalue\", \r\n" + "        			\"DataType\": \"string\" \r\n"
			+ "       			},  \r\n" + "       			{\r\n"
			+ " 					\"AttributeId\": \"ns:c3isp:dpo-metadata\", \r\n"
			+ "        			\"Value\": \"<DPOS METADATA STRING>\", \r\n"
			+ "        			\"DataType\": \"string\" \r\n" + "       			}\r\n" + "     		]}\r\n"
			+ "		}";

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
		return restTemplateBuilder.basicAuthorization(restUser, restPassword).build();
	}

	private final static Logger log = LoggerFactory.getLogger(ApiCreateDPO.class);

	/**
	 * Sample POST method
	 *
	 * @param pathParam  a parameter extracted from path like /template/PARAM_VALUE/
	 *                   it has to match the {param} from the path
	 * @param headerdata extracted from the path
	 * @param data       is extracted from the request payload
	 * @return just a string
	 */

	@ApiResponses(value = { @ApiResponse(code = 412, message = "Client Error"),
			@ApiResponse(code = 500, message = "Internal server error"),
			@ApiResponse(code = 200, message = "Created succesfully") })

	@ApiOperation(httpMethod = "POST", value = "Create a new DPO", tags = "ISI_API", notes = "it returns a DPO-id, \n\n"
			+ EXAMPLERESTMETADATA)

	/**
	 *
	 *
	 * @param input_metadata
	 * @param fileToSubmit
	 * @return
	 * @throws JsonProcessingException
	 */

	@RequestMapping(method = RequestMethod.POST, value = "/dpo", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<RestResponse> create(
			@RequestPart(value = "input_metadata") @ApiParam(defaultValue = EXAMPLERESTMETADATA, example = EXAMPLERESTMETADATA) String input_metadata,
			@ApiParam(required = false, value = "does the submitted data need to be normalised?", allowableValues = "true,false") @RequestParam(name = "norm", required = false, defaultValue = "false") String normalization,
			@RequestPart(value = "fileToSubmit") MultipartFile fileToSubmit) throws JsonProcessingException {

		log.info("I received a POST request, metadata: " + input_metadata);

		try {

			String tmpFilePath = "/tmp/" + UUID.randomUUID();
			fileToSubmit.transferTo(new File(tmpFilePath));

			if (Boolean.parseBoolean(normalization)) {
				tmpFilePath = convertToStix(tmpFilePath);
			}

			String vaultStringResponse = vaultWrapper.getHttpUtils().getCompleteDataKeyResponse();
			VaultResponse vaultResponse = null;
			vaultResponse = objectMapper.readValue(vaultStringResponse, VaultResponse.class);
			VaultDatakey datakey = vaultResponse.getDatakey();

			String outputCipheredFile = vaultWrapper.getCipherOperations().performOperation(datakey, tmpFilePath,
					"encrypt");

			CreateBodyParams body = new CreateBodyParams(input_metadata, outputCipheredFile, datakey.getPlaintext());
			URI publishUri = UriComponentsBuilder.fromUriString(dsaAdapterFrontendURL).path("/v1/create").build()
					.toUri();
			ResponseEntity<RestResponse> response = restTemplate.postForEntity(publishUri, body, RestResponse.class);

			return response;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public String convertToStix(String tmpFilePath) throws IOException {
		String formatApi = "/convert";
		MultiValueMap<String, Object> body = new LinkedMultiValueMap<String, Object>();
		body.add("file", new FileSystemResource(new File(tmpFilePath)));
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<MultiValueMap<String, Object>>(body,
				headers);
		ResponseEntity<ByteArrayResource> response = restTemplate.exchange(formatAdapterURL + formatApi,
				HttpMethod.POST, request, ByteArrayResource.class);

		tmpFilePath = "/tmp/stixed-" + tmpFilePath.split("/tmp/")[1];
		OutputStream os = new FileOutputStream(tmpFilePath);
		os.write(response.getBody().getByteArray());
		os.close();
		return tmpFilePath;
	}

	public ResponseEntity sendCreateToFrontEnd(String dposMetadata, MultipartFile fileToSubmit, boolean normalize) {

		try {

			System.err.println("received file size is: " + fileToSubmit.getBytes().length + " file name: "
					+ fileToSubmit.getName() + " fileName: " + fileToSubmit.getOriginalFilename());

			if (fileToSubmit.getBytes().length == 0) {
				log.error("error reading input file: size is 0?");
				ResponseEntity<String> thisresponse = new ResponseEntity<String>(
						"{\"message\":\"error reading input file\"}", HttpStatus.PRECONDITION_FAILED);
				return thisresponse;
			}
		} catch (IOException e) {
			log.error("file not submitted or its size is 0");
			e.printStackTrace();

			ResponseEntity<String> thisresponse = new ResponseEntity<String>(
					"{\"message\":\"file not submitted or its size is 0\"", HttpStatus.PRECONDITION_FAILED);
			return thisresponse;
		}

		FileSystemResource res = null;
		File tmpFile = null;

		try {
			File intermediateFile = inputStreamToTempFile(fileToSubmit.getInputStream(),
					fileToSubmit.getOriginalFilename());

			if (normalize) {
				String formatApi = "/convert";
				MultiValueMap<String, Object> parts = new LinkedMultiValueMap<String, Object>();
				parts.add("file", new FileSystemResource(intermediateFile));

				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.MULTIPART_FORM_DATA);
				HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<MultiValueMap<String, Object>>(parts,
						headers);

				ResponseEntity<ByteArrayResource> response = restTemplate.exchange(formatAdapterURL + formatApi,
						HttpMethod.POST, request, ByteArrayResource.class);

				ByteArrayResource file = response.getBody();

				tmpFile = File.createTempFile("create-inputfile", "tmp");
				tmpFile.deleteOnExit();
				FileUtils.forceDeleteOnExit(tmpFile);
				FileUtils.copyInputStreamToFile(file.getInputStream(), tmpFile);

			} else {
				tmpFile = intermediateFile;
			}

			res = new FileSystemResource(tmpFile);

		} catch (IOException e) {
			e.printStackTrace();
			log.error("file input processing issue", e);

			ResponseEntity<String> thisresponse = new ResponseEntity<String>("file input processing issue",
					HttpStatus.INTERNAL_SERVER_ERROR);
			return thisresponse;
		}

		String vaultStringResponse = vaultWrapper.getHttpUtils().getCompleteDataKeyResponse();
		VaultResponse vaultResponse = null;
		try {
			vaultResponse = objectMapper.readValue(vaultStringResponse, VaultResponse.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		VaultDatakey datakey = vaultResponse.getDatakey();

		String outputCipheredFile = vaultWrapper.getCipherOperations().performOperation(datakey, res.getPath(),
				"encrypt");

		MultiValueMap<String, Object> parts = new LinkedMultiValueMap<String, Object>();

		parts.add("input_metadata", dposMetadata);

		parts.add("fileToSubmit", new FileSystemResource(new File(outputCipheredFile)));

		URI publishUri = UriComponentsBuilder.fromUriString(dsaAdapterFrontendURL).path("/v1/create").build().toUri();

		ResponseEntity<RestResponse> response = null;

		try {
			response = restTemplate.postForEntity(publishUri, parts, RestResponse.class);
		} catch (RestClientException clientEx) {
//		if (response.getStatusCode() == HttpStatus.FORBIDDEN) {
			HttpStatus status = null;

			if (clientEx instanceof HttpClientErrorException) {
				HttpClientErrorException ex = (HttpClientErrorException) clientEx;
				status = ex.getStatusCode();
			}

			ResponseEntity thisresponse = new ResponseEntity<String>("unauthorised",
					(status == null) ? HttpStatus.FORBIDDEN : status);
			return thisresponse;
		}

		vaultWrapper.getCipherOperations().performOperation(datakey, outputCipheredFile, "decrypt");

		return new ResponseEntity<RestResponse>(response.getBody(), HttpStatus.OK);

	}

	private File inputStreamToTempFile(InputStream inputStream, String originalFilename) throws IOException {
		File tmpFile;
		tmpFile = File.createTempFile(FilenameUtils.getBaseName(originalFilename) + "----",
				"." + FilenameUtils.getExtension(originalFilename));
		tmpFile.deleteOnExit();

		FileUtils.forceDeleteOnExit(tmpFile);
		FileUtils.copyInputStreamToFile(inputStream, tmpFile);
		return tmpFile;
	}

}
