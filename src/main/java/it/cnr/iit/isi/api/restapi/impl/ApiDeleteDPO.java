/**
 *  Copyright 2017 Hewlett Packard Enterprise Development Company, L.P.
 */
package it.cnr.iit.isi.api.restapi.impl;

import java.net.URI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import it.cnr.iit.isi.api.restapi.types.RestResponse;
import it.cnr.iit.isi.api.restapi.types.xacml.RequestContainer;

@ApiModel(value = "Template", description = "Template of REST APIs")
@RestController
@RequestMapping("/v1")
public class ApiDeleteDPO {

	/**
	 * These two are examples of configuration extracted from the
	 * application.properties file under the src/main/resources. Springboot will
	 * automatically load the file and its values, if file is not in the local path,
	 * it can be in the java classpath or set as an environment variable
	 */

	@Value("${rest.endpoint.url.callGet}")
	private String callGetEndpoint;

	@Value("${security.user.name}")
	private String restUser;
	@Value("${security.user.password}")
	private String restPassword;

	@Value("${dsa.adapter.frontend.endpoint}")
	private String dsaAdapterFrontendURL;

	/**
	 * Used to call REST endpoints; configured by RestTemplateBuilder
	 */
	@Autowired
	private RestTemplate restTemplate;

	/**
	 * Logger class, can be used within the methods to log information
	 */
	private final static Logger LOGGER = LoggerFactory.getLogger(ApiDeleteDPO.class);

	/**
	 * an example of DELETE
	 */
	@ApiOperation(httpMethod = "DELETE", tags = "ISI_API", response = RestResponse.class, value = "Delete a DPO identified by DPO_ID")
	@RequestMapping(method = RequestMethod.DELETE, value = "/dpo/{dpo_id}/", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RestResponse> delete(
			@PathVariable("dpo_id") @ApiParam(name = "dpo_id", value = "DPO to be deleted", allowEmptyValue = false) String dpo_id,
			@RequestBody() @ApiParam() RequestContainer input_metadata) {

		return sendDeletetoFrontEnd(dpo_id);
	}

	public ResponseEntity sendDeletetoFrontEnd(String dpo_id) {

		URI publishUri = UriComponentsBuilder.fromUriString(dsaAdapterFrontendURL).path("/v1/dpo").pathSegment(dpo_id)
				.build().toUri();

		ResponseEntity<RestResponse> response = null;

		try {
			response = restTemplate.exchange(publishUri, HttpMethod.DELETE, null, RestResponse.class);
		} catch (RestClientException clientEx) {
//		if (response.getStatusCode() == HttpStatus.FORBIDDEN) {
			HttpStatus status = null;

			if (clientEx instanceof HttpClientErrorException) {
				HttpClientErrorException ex = (HttpClientErrorException) clientEx;
				status = ex.getStatusCode();
			}

			ResponseEntity thisresponse = new ResponseEntity<String>("unauthorised",
					(status == null) ? HttpStatus.FORBIDDEN : status);
			return thisresponse;
		}

		return new ResponseEntity<RestResponse>(response.getBody(), HttpStatus.OK);

	}

}
