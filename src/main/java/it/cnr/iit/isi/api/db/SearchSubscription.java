//package eu.c3isp.isi.api.db;
//
//import com.j256.ormlite.field.DataType;
//import com.j256.ormlite.field.DatabaseField;
//import com.j256.ormlite.table.DatabaseTable;
//
//import eu.c3isp.isi.api.restapi.types.xacml.RequestContainer;
//
//@DatabaseTable(tableName = "dpo_info_table")
//public class SearchSubscription {
////	private static final Gson gson = new Gson();
//	public static final String ID_FIELD = "searchId";
//	public static final String METADATA_FIELD = "metadata";
//	public static final String PERIOD_FIELD = "period";
//	public static final String SEARCH_PARAMS_FIELD = "search_params";
//	public static final String SRC_URI = "srcUri";
//
//	@DatabaseField(id = true, columnName = ID_FIELD)
//	private Integer searchId;
//
//	@DatabaseField(columnName = SEARCH_PARAMS_FIELD, dataType = DataType.SERIALIZABLE)
//	private RequestContainer searchParams;
//
//	@DatabaseField(columnName = METADATA_FIELD, dataType = DataType.SERIALIZABLE)
//	private RequestContainer metadata;
//
//	@DatabaseField(columnName = PERIOD_FIELD)
//	private String period;
//
//	@DatabaseField(columnName = SRC_URI)
//	private String src;
//
//	public SearchSubscription(Integer searchId, RequestContainer searchParams, RequestContainer metadata, String src,
//			String period) {
//		this.setSearchId(searchId);
//		this.setSearchParams(searchParams);
//		this.setMetadata(metadata);
//		this.setSrc(src);
//		this.setPeriod(period);
//	}
//
//	public Integer getSearchId() {
//		return searchId;
//	}
//
//	public void setSearchId(Integer searchId) {
//		this.searchId = searchId;
//	}
//
//	public RequestContainer getSearchParams() {
//		return searchParams;
//	}
//
//	public void setSearchParams(RequestContainer searchParams) {
//		this.searchParams = searchParams;
//	}
//
//	public RequestContainer getMetadata() {
//		return metadata;
//	}
//
//	public void setMetadata(RequestContainer metadata) {
//		this.metadata = metadata;
//	}
//
//	public String getPeriod() {
//		return period;
//	}
//
//	public void setPeriod(String period) {
//		this.period = period;
//	}
//
//	public String getSrc() {
//		return src;
//	}
//
//	public void setSrc(String src) {
//		this.src = src;
//	}
//}
