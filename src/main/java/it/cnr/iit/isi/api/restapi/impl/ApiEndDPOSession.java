/**
 *  Copyright 2017 Hewlett Packard Enterprise Development Company, L.P.
 */
package it.cnr.iit.isi.api.restapi.impl;

import java.net.URI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import it.cnr.iit.isi.api.restapi.types.ResponseEvent;
import it.cnr.iit.isi.api.restapi.types.RestResponse;

@ApiModel(value = "EndSession", description = "API to End a DPO processing session")
@RestController
@RequestMapping("/v1")
public class ApiEndDPOSession {

	/**
	 * These two are examples of configuration extracted from the
	 * application.properties file under the src/main/resources. Springboot will
	 * automatically load the file and its values, if file is not in the local path,
	 * it can be in the java classpath or set as an environment variable
	 */

	@Value("${rest.endpoint.url.callGet}")
	private String callGetEndpoint;

	@Value("${security.user.name}")
	private String restUser;
	@Value("${security.user.password}")
	private String restPassword;

	@Value("${dsa.adapter.frontend.endpoint}")
	private String dsaAdapterFrontendURL;

	/**
	 * Used to call REST endpoints; configured by RestTemplateBuilder
	 */
	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private ObjectMapper mapper;

	/**
	 * Logger class, can be used within the methods to log information
	 */
	private final static Logger LOGGER = LoggerFactory.getLogger(ApiEndDPOSession.class);

	/**
	 * an example of DELETE
	 */
	@ApiOperation(httpMethod = "POST", tags = "ISI_API", response = RestResponse.class, value = "End a DPO Session identified by sessionID")
	@RequestMapping(method = RequestMethod.POST, value = "/releaseData/{sessionId}/"/*
																					 * , produces = MediaType.
																					 * APPLICATION_JSON_VALUE, consumes
																					 * = MediaType.
																					 * APPLICATION_JSON_VALUE
																					 */)
	public ResponseEntity releaseData(
			@PathVariable("sessionId") @ApiParam(name = "sessionId", value = "Session DPO to be stopped", allowEmptyValue = false) String sessionId) {

		LOGGER.info("received EndSession request, session:" + sessionId);

		return sendDeletetoFrontEnd(sessionId);
	}

	public ResponseEntity sendDeletetoFrontEnd(String session_id) {

		URI publishUri = UriComponentsBuilder.fromUriString(dsaAdapterFrontendURL).path("/v1/releaseData")
				.pathSegment(session_id).build().toUri();

		ResponseEntity<ResponseEvent> response = null;

		try {
			response = restTemplate.exchange(publishUri, HttpMethod.POST, null, ResponseEvent.class);
		} catch (RestClientException clientEx) {
//		if (response.getStatusCode() == HttpStatus.FORBIDDEN) {
			HttpStatus status = null;

			if (clientEx instanceof HttpClientErrorException) {
				HttpClientErrorException ex = (HttpClientErrorException) clientEx;
				status = ex.getStatusCode();
			}

			ResponseEntity thisresponse = new ResponseEntity<String>("unauthorised",
					(status == null) ? HttpStatus.FORBIDDEN : status);
			return thisresponse;
		}

		return response;

	}

}
