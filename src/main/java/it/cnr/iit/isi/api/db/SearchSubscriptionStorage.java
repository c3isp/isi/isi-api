//package eu.c3isp.isi.api.db;
//
//import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.logging.Logger;
//
//import javax.annotation.PostConstruct;
//
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Component;
//
//import com.j256.ormlite.dao.Dao;
//import com.j256.ormlite.dao.DaoManager;
//import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
//import com.j256.ormlite.stmt.QueryBuilder;
//import com.j256.ormlite.support.ConnectionSource;
//import com.j256.ormlite.table.TableUtils;
//
//@Component
//public class SearchSubscriptionStorage {
//
//	private Logger log = Logger.getLogger(SearchSubscriptionStorage.class.getName());
//
//	private ConnectionSource connection = null;
//	private Dao<SearchSubscription, Integer> dao = null;
//
//	@Value("${db.connection:jdbc:sqlite::memory:}")
//	private String dbConnection;
//
//	@PostConstruct
//	public void init() {
//		refresh();
//	}
//
//	private synchronized void refresh() {
//		if (connection == null || !connection.isOpen()) {
//			try {
//				connection = new JdbcPooledConnectionSource(dbConnection);
//				dao = DaoManager.createDao(connection, SearchSubscription.class);
//				TableUtils.createTableIfNotExists(connection, SearchSubscription.class);
//				log.info(() -> this.getClass().getSimpleName() + " database connection established.");
//			} catch (SQLException e) {
//				log.severe(() -> e.getClass().getSimpleName() + " while refreshing connection : " + e.getMessage());
//			}
//		}
//	}
//
//	public boolean createOrUpdate(SearchSubscription searchSub) {
//		try {
//			refresh();
//			dao.createOrUpdate(searchSub);
//			return true;
//		} catch (SQLException e) {
//			log.severe(() -> e.getClass().getSimpleName() + " : " + e.getMessage());
//			return false;
//		}
//	}
//
//	public boolean delete(SearchSubscription searchSub) {
//		try {
//			refresh();
//			dao.delete(searchSub);
//			return true;
//		} catch (SQLException e) {
//			log.severe(() -> e.getClass().getSimpleName() + " : " + e.getMessage());
//			return false;
//		}
//	}
//
//	private SearchSubscription getForField(String column, String value) {
//		List<SearchSubscription> tables = getForFields(column, value);
//		if (tables == null || tables.size() > 1) {
//			throw new IllegalStateException("Same field used multiple times");
//		}
//		return tables.stream().findFirst().orElse(null);
//	}
//
//	private SearchSubscription getForField(String column, Integer value) {
//		List<SearchSubscription> tables = getForFields(column, value);
//		if (tables == null || tables.size() > 1) {
//			throw new IllegalStateException("Same field used multiple times");
//		}
//		return tables.stream().findFirst().orElse(null);
//	}
//
//	public List<SearchSubscription> getForFields(String column, String value) {
//		try {
//			refresh();
//			QueryBuilder<SearchSubscription, Integer> qbAttributes = dao.queryBuilder();
//			return qbAttributes.where().eq(column, value).query();
//		} catch (Exception e) {
//			log.severe(() -> e.getClass().getSimpleName() + " : " + column + " : " + value + " , " + e.getMessage());
//		}
//		return new ArrayList<>();
//	}
//
//	public List<SearchSubscription> getForFields(String column, Integer value) {
//		try {
//			refresh();
//			QueryBuilder<SearchSubscription, Integer> qbAttributes = dao.queryBuilder();
//			return qbAttributes.where().eq(column, value).query();
//		} catch (Exception e) {
//			log.severe(() -> e.getClass().getSimpleName() + " : " + column + " : " + value + " , " + e.getMessage());
//		}
//		return new ArrayList<>();
//	}
//
//}
