package it.cnr.iit.isi.api.restapi.types;

import java.io.IOException;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

//Crazy! if uncommented, the two classes get "merged" in the swagger view!
//@ApiModel(value="RestMetadata",description="Required metadata to complete the operation")
//@XmlRootElement(name ="RestMetadata")
public class SearchParams {

	private String combining_rule;

    private List<Criterion> criteria;
	
/**
 * Methods
 */

	@ApiModelProperty(
			required=true, 
			value = "combining rule used to compute the specified criteria", 
			allowEmptyValue=false)	
    public String getCombiningRule() {
		return combining_rule;
	}

	public void setCombiningRule(String combiningRule) {
		this.combining_rule = combiningRule;
	}

	@ApiModelProperty(
			required=true, 
			value = "List of criteria for the search operation", 
			allowEmptyValue=true)
	public List<Criterion> getCriteria() {
		return criteria;
	}

	public void setCriteria(List<Criterion> criteria) {
		this.criteria = criteria;
	}

        
    @Override
    public String toString() {
    	
    	StringWriter toReturn =  new StringWriter();
    	ObjectMapper mapper = new ObjectMapper();
    	try {
			mapper.writeValue(toReturn, this);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
    	return toReturn.toString();
    	
    } 
}