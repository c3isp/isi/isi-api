/**
 *  Copyright 2017 Hewlett Packard Enterprise Development Company, L.P.
 */
package it.cnr.iit.isi.api.restapi.impl;

import java.net.URI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import it.cnr.iit.isi.api.restapi.types.RestResponse;
import it.cnr.iit.isi.api.restapi.types.xacml.RequestContainer;

@ApiModel(value = "Template", description = "Template of REST APIs")
@RestController
@RequestMapping("/v1")
public class ApiMoveDPO {

	/**
	 * These two are examples of configuration extracted from the
	 * application.properties file under the src/main/resources. Springboot will
	 * automatically load the file and its values, if file is not in the local path,
	 * it can be in the java classpath or set as an environment variable
	 */

	@Value("${dsa.adapter.frontend.endpoint}")
	private String dsaAdapterFrontendURL;

	@Value("${rest.endpoint.url.callGet}")
	private String callGetEndpoint;

	@Value("${security.user.name}")
	private String restUser;
	@Value("${security.user.password}")
	private String restPassword;

	/**
	 * Used to call REST endpoints; configured by RestTemplateBuilder
	 */
	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	ApplicationContext ctx;

	@Autowired
	ObjectMapper mapper;

	/**
	 * Logger class, can be used within the methods to log information
	 */
	private final static Logger LOGGER = LoggerFactory.getLogger(ApiMoveDPO.class);

	/**
	 * Move uses POST to permit the input of metadata potentially required
	 */
	@ApiOperation(httpMethod = "POST", value = "Moves a DPO -- part 1: sending ISI", notes = "Same params as for read DPO", response = RestResponse.class, tags = "ISI_API")

	@ApiImplicitParams({
			@ApiImplicitParam(name = "X-c3isp-input_metadata", value = "Metadata in format RestMetadata, IDENTICAL TO THOSE YOU WOULD USE FOR THE READ OPERATION\n\n\n"
					+ ApiReadDPO.EXAMPLEMETADATA, required = true, dataTypeClass = RequestContainer.class, paramType = "header"),
//        @ApiImplicitParam(name = "X-myheader-attr2", value = "X-myheader-attr2", required = false, dataType = "string", paramType = "header")
	})

	@RequestMapping(method = RequestMethod.POST, value = "/move/dpo/{dpo_id}/",
//			consumes =  MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)

	public ResponseEntity<RestResponse> move(
//			@PathVariable("dpo_id") String cti_id,
//			@RequestBody() RequestContainer input_metadata) {
			@RequestHeader(value = "X-c3isp-input_metadata") @ApiParam(required = false, allowEmptyValue = false, value = "Metadata in format RestMetadata, IDENTICAL TO THOSE YOU WOULD USE FOR THE READ OPERATION") String input_metadata,
			@RequestHeader(value = "X-c3isp-destination") @ApiParam(required = true, allowEmptyValue = false, example = "https://isic3isp.iit.cnr.it:8443/isi-api/api/v1/", value = "set destination as the full address of the ISI API you want to use as destination, without method specifications, ex: https://isic3isp.iit.cnr.it:8443/isi-api/v1/") String destination,
			@PathVariable("dpo_id") String dpo_id, Authentication authentication) {

		/**
		 * on the local ISI: - have a "move" operation triggered, so DMOs etc can be
		 * enforced on the central ISI: - a plain create, for the time being, - TODO
		 * change to an extra parameter to report that this is not a create so don't
		 * apply create but move obligations
		 */

		UserDetails userDetails = (UserDetails) authentication.getPrincipal();

		try {
			RestResponse res = sendMovetoFrontEnd(input_metadata, dpo_id, destination);
			return new ResponseEntity<RestResponse>(res, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	public RestResponse sendMovetoFrontEnd(String input_metadata, String dpo_id, String destination)
			throws RestClientException, Exception {

		URI moveUri = UriComponentsBuilder.fromUriString(dsaAdapterFrontendURL).path("/v1/move/dpo").pathSegment(dpo_id)
				.build().toUri();

		MultiValueMap<String, Object> parts = new LinkedMultiValueMap<String, Object>();

		parts.add("input_metadata", input_metadata);
		parts.add("destination", destination);

		ResponseEntity<RestResponse> response = null;

		try {
			response = restTemplate.postForEntity(moveUri, parts, RestResponse.class);
		} catch (RestClientException clientEx) {
//		if (response.getStatusCode() == HttpStatus.FORBIDDEN) {
			HttpStatus status = null;

			if (clientEx instanceof HttpClientErrorException) {
				HttpClientErrorException ex = (HttpClientErrorException) clientEx;
				status = ex.getStatusCode();
			}

			throw new Exception(String.valueOf(HttpStatus.FORBIDDEN.value()));
		}

		return response.getBody();

	}

}
