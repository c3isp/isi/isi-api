/**
 *  Copyright 2017 Hewlett Packard Enterprise Development Company, L.P.
 */
package it.cnr.iit.isi.api.restapi.impl;

import java.io.File;
import java.io.IOException;
import java.net.URI;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import it.cnr.iit.isi.api.restapi.types.xacml.RequestContainer;

@ApiModel(value = "Template", description = "Template of REST APIs")
@RestController
@RequestMapping("/v1")
public class ApiReadDPO {

	/**
	 * These two are examples of configuration extracted from the
	 * application.properties file under the src/main/resources. Springboot will
	 * automatically load the file and its values, if file is not in the local path,
	 * it can be in the java classpath or set as an environment variable
	 */

	@Value("${dsa.adapter.frontend.endpoint}")
	private String dsaAdapterFrontendURL;

	@Value("${rest.endpoint.url.callGet}")
	private String callGetEndpoint;

	@Value("${security.user.name}")
	private String restUser;
	@Value("${security.user.password}")
	private String restPassword;

	/**
	 * Used to call REST endpoints; configured by RestTemplateBuilder
	 */
	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private ObjectMapper mapper;

	public static final String EXAMPLEMETADATA = "\n\n" + "		{\r\n" + "			\"Request\": {\r\n"
			+ "				\"Attribute\": [\r\n" + "						{ \r\n"
			+ "							\"AttributeId\": \"urn:oasis:names:tc:xacml:3.0:subject:access-purpose\", \r\n"
			+ "        					\"Value\": \"generic\", \r\n"
			+ "        					\"DataType\": \"string\" \r\n" + "       					}\r\n"
			+ "     			]\r\n" + "			}\r\n" + "		}\r\n\n\n";

	/**
	 * Logger class, can be used within the methods to log information
	 */
	private final static Logger LOGGER = LoggerFactory.getLogger(ApiReadDPO.class);

	@ApiOperation(httpMethod = "GET", value = "Get an existing DPO content identified by its DPO_ID", tags = "ISI_API", notes = "Return the stored content in extracted DPO, in the HTTP body.\n"
			+ "MANDATORY PARAMETER: urn:oasis:names:tc:xacml:3.0:subject:access-purpose."
			+ " Additional parameters can be added as well as deemed necessary." + "Input format is: \n\n"
			+ EXAMPLEMETADATA)
	@RequestMapping(method = RequestMethod.GET, value = "/dpo/{dpo_id}/", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)

	@ApiImplicitParams({
			@ApiImplicitParam(name = "X-c3isp-input_metadata", value = EXAMPLEMETADATA, required = false, dataTypeClass = RequestContainer.class, paramType = "header"),

	})
	public ResponseEntity<FileSystemResource> get(
			@RequestHeader(value = "X-c3isp-input_metadata") @ApiParam(required = false, allowEmptyValue = false, value = "Metadata in format RestMetadata") String input_metadata,
			@PathVariable("dpo_id") String cti_id) {

		try {
			FileSystemResource res = new FileSystemResource(sendReadtoFrontEnd(input_metadata, cti_id));
			return new ResponseEntity<FileSystemResource>(res, HttpStatus.OK);
		} catch (Exception clientEx) {

			HttpStatus status = null;

			if (clientEx instanceof HttpClientErrorException) {
				HttpClientErrorException ex = (HttpClientErrorException) clientEx;
				status = ex.getStatusCode();
			}

			ResponseEntity thisresponse = new ResponseEntity<String>(
					(status == null) ? "unauthorised" : clientEx.getMessage(),
					(status == null) ? HttpStatus.FORBIDDEN : status);
			return thisresponse;
		}
	}

	public File sendReadtoFrontEnd(String input_metadata, String dpo_id) {

		URI readUri = UriComponentsBuilder.fromUriString(dsaAdapterFrontendURL).path("/v1/dpo").pathSegment(dpo_id)
				.build().toUri();

		MultiValueMap<String, Object> parts = new LinkedMultiValueMap<String, Object>();

		parts.add("input_metadata", input_metadata);

		HttpHeaders headers = new HttpHeaders();

		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<MultiValueMap<String, Object>>(parts,
				headers);

		ResponseEntity<ByteArrayResource> response = null;

		try {
			response = restTemplate.postForEntity(readUri, request, ByteArrayResource.class);
			;
		} catch (RestClientException clientEx) {
			throw clientEx;
		}

		return copyToLocalFile(response, LOGGER);

	}

	File copyToLocalFile(ResponseEntity<ByteArrayResource> req, Logger LOGGER) {
		byte[] responseContent = req.getBody().getByteArray();
		File tmpFile;
		try {
			tmpFile = File.createTempFile("test", "tst");
			tmpFile.deleteOnExit();
			FileUtils.forceDeleteOnExit(tmpFile);
			FileUtils.writeByteArrayToFile(tmpFile, responseContent);
		} catch (IOException e) {
			LOGGER.error("error creating tempfile", e);
			e.printStackTrace();
			return null;
		}
		return tmpFile;
	}
}