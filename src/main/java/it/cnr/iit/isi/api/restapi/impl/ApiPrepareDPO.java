/**
 *  Copyright 2017 Hewlett Packard Enterprise Development Company, L.P.
 */
package it.cnr.iit.isi.api.restapi.impl;

import java.net.URI;

import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import it.cnr.iit.isi.api.restapi.types.xacml.RequestContainer;

@ApiModel(value = "Template", description = "Template of REST APIs")
@RestController
@RequestMapping("/v1")
public class ApiPrepareDPO {

	private static final String BUFFERMANAGER_HEADER_METADATA = "X-c3isp-metadata";
	private static final String DSAADAPTER_HEADER_METADATA = "X-c3isp-input_metadata";

	/**
	 * These two are examples of configuration extracted from the
	 * application.properties file under the src/main/resources. Springboot will
	 * automatically load the file and its values, if file is not in the local path,
	 * it can be in the java classpath or set as an environment variable
	 */

	@Value("${buffer.manager.endpoint}")
	private String bufferManagerAPI;

	@Value("${dsa.adapter.frontend.endpoint}")
	private String dsaAdapterFrontendURL;

	@Value("${security.user.name}")
	private String restUser;
	@Value("${security.user.password}")
	private String restPassword;

	/**
	 * Used to call REST endpoints; configured by RestTemplateBuilder
	 */
	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	ObjectMapper mapper;

	/**
	 * Logger class, can be used within the methods to log information
	 */
	private final static Logger LOGGER = LoggerFactory.getLogger(ApiPrepareDPO.class);

	private final static String BM_RESPONSE = "		{\r\n" + " 		\"data\": [\r\n" + "			{\r\n"
			+ "				\"DPO_id\": \"mail1\",\r\n" + "				\"code\": 201,\r\n"
			+ "				\"file\": \"file:///opt/isi/datalakebuffer/5ee94fa3-c408-43c9-a031-588f03240f1e/mail1.dpo\",\r\n"
			+ "				\"message\": \"Success.\"\r\n" + "    		}\r\n" + "  		],\r\n"
			+ "  		\"datalake\": {\r\n"
			+ "    		\"URI\": \"file:///opt/isi/datalakebuffer/1a3d88d1-07fc-47b6-8579-5c07107b9302\",\r\n"
			+ "    		\"message\": \"string\"\r\n" + "  		}\r\n" + "		}";

	private final static String EXAMPLEPrepDataRESTMETADATA = ""

			+ "	{ \r\n" + "		\"Request\": { \r\n" + "      		\"Attribute\": [{ \r\n"
			+ "        	\"AttributeId\": \"ns:c3isp:aggregation-operation\", \r\n"
			+ "        	\"Value\": \"somevalue\", \r\n" + "        	\"DataType\": \"string\" \r\n" + "		}, { \r\n"
			+ "			\"AttributeId\": \"urn:oasis:names:tc:xacml:1.0:action:iai:session-id\", \r\n"
			+ "        	\"Value\": \"somevalue\", \r\n" + "        	\"DataType\": \"string\" \r\n" + "		}]}\r\n"
			+ "	}";

	@ApiOperation(httpMethod = "POST", value = "Prepare a new VDL populated by a set of existing DPOs identified by DPO_IDs, the method returns a "
			+ "reference to the prepared data in a structure as defined by Buffer Manager", tags = "ISI_API",
//			response = RestResponse.class,
			notes = "same answer as BufferManager.prepareData: \n\n " + BM_RESPONSE)
	@RequestMapping(method = RequestMethod.POST, value = "/prepareData", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)

	@ApiImplicitParams({
			@ApiImplicitParam(name = BUFFERMANAGER_HEADER_METADATA, value = "The metatadata format is as follows: \n\n"
					+ EXAMPLEPrepDataRESTMETADATA, required = false, dataTypeClass = RequestContainer.class, paramType = "header"), })

	public ResponseEntity prepareDPO(
//			@PathVariable("dpo_id") String cti_id,
			@RequestHeader(value = BUFFERMANAGER_HEADER_METADATA) @ApiParam(required = true, allowEmptyValue = false) RequestContainer input_metadata,
			@RequestParam(name = "serviceName", required = true) String serviceName,
			@RequestParam(name = "format", required = true) @ApiParam(allowableValues = "EML,CEF,MODEL,PCAP,BINARY") String format,
			@RequestParam(name = "data_lake", required = true) @ApiParam(allowableValues = "DLB,VDL") String dataLake,
			@RequestParam(name = "type", required = true) @ApiParam(allowableValues = "FS,MYSQL,HDFS") String type,
			@RequestBody() @NotNull @ApiParam(example = "[dposID_1, dposID_2,...]") String[] dpoIDs) {

		if (input_metadata != null) {
			LOGGER.info("element: " + input_metadata.toString());
		} else {
			LOGGER.info("element was received as null ");
		}

		/**
		 * to extract urn:oasis:names:tc:xacml:1.0:action:iai:session-id
		 *
		 * multiSessionId === iaiSessionID
		 *
		 *
		 * isi:multi_authz=cae.tryAccessMulti(multiSessionId)
		 *
		 * only after,
		 *
		 * isi:bm.prepareData(isi_metadata*+multiSessionId, DPO_ids, bm_metadata)
		 */

		MultiValueMap<String, Object> parts = new LinkedMultiValueMap<String, Object>();

		HttpHeaders headers = new HttpHeaders();
//		headers.add("Authorization", "Basic " + base64Creds);
		try {
			headers.add(DSAADAPTER_HEADER_METADATA, mapper.writeValueAsString(input_metadata));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			LOGGER.error("error in processing input_metadata", e);
			return new ResponseEntity("metadata parsing error", HttpStatus.PRECONDITION_FAILED);
		}

		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity request = new HttpEntity(dpoIDs, headers);

		URI prepareDataDSAAdapterhUri = UriComponentsBuilder.fromUriString(dsaAdapterFrontendURL)
				.path("/v1/prepareData").queryParam("serviceName", serviceName).queryParam("format", format)
				.queryParam("data_lake", dataLake).queryParam("type", type).build().toUri();

		ResponseEntity<String> response = restTemplate.exchange(prepareDataDSAAdapterhUri, HttpMethod.POST, request,
				String.class);

		LOGGER.info("DSA Adapter Front-End prepareData answer: " + response.getBody());

		/**
		 * TODO add response parsing
		 */

		return new ResponseEntity(response.getBody(), response.getHeaders(), response.getStatusCode());
	}
}
