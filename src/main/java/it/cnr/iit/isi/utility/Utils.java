package it.cnr.iit.isi.utility;

import java.io.File;
import java.util.UUID;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class Utils {

	public String writeFile(MultipartFile inputFile) {
		try {
			String outputFile = "/tmp/" + UUID.randomUUID() + inputFile.getOriginalFilename();
			File file = new File(outputFile);
			inputFile.transferTo(file);
			return outputFile;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}
}
