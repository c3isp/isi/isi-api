# README
ISI API. Public entry-point for ISI functionalities.

## Compile project and create WAR file
    mvn clean package

## Execute project

* run ApplicationDeployer class, for example, from Eclipse *preferred*
*	```mvn tomcat7:run (to be verified)``` *old method*



## Changelog

### 1.2.2-ALPHA

fixed bug in MoveAPI

### 1.2.0-ALPHA

updated swagger documentation

### 1.1.0-ALPHA

added EndSession

### 1.0.19-ALPHA

fixed a bug in search parameters in Search API

### 1.0.18-ALPHA

Fixed a bug in PrepareData call to BufferManager's PrepareData

### 1.0.17-ALPHA

Updated springboot to 1.5.21

### 1.0.16-ALPHA

update to match dsa-frontend-adapter 0.5.1 ApiRead signature

### 1.0.15-ALPHA

adding delete operation after move to delete the transferred DPO


### 1.0.14-ALPHA

bug fix

### 1.0.13-ALPHA

new Move and Transfer logic

### 1.0.12-ALPHA

revised PrepareData flow

### 1.0.11-ALPHA

Initial support for MOVE operation

### 1.0.10-ALPHA

configuration fixing attempt, max-upload-file

### 1.0.9-ALPHA

modified file name for submission to format adapter, to match template:

* <original file name>----<random id>.<original extension> 


### 1.0.8-ALPHA

bug fix

### 1.0.7-ALPHA

integration with format adapter


### 1.0.6-ALPHA

restored the custom attribute support, updates to swagger documentation

### 1.0.5-ALPHA

bug fixing

### 1.0.4-ALPHA

Update of prepareDPO description and implementation of a workaround for fixing Buffer Manager incompatibility problem

### 1.0.3-ALPHA

fixed another bug in calling prepareDPO of BufferManager

### 1.0.2-ALPHA

fixed bug in calling prepareDPO of BufferManager


### 1.0.1-ALPHA

small updates

### 1.0.0-ALPHA

initial, complete basic implementation

### 0.1.5

bug fixing

### 0.1.4

connection to dsa adapter frontend, refined API specification and metadata 

### 0.1.3

Updated the parameters of methods, according to agreement

```
{ 
  "Request": { 
      "Attribute": [{ 
        "AttributeId": "ns:c3isp:dsa-id", 
        "Value": "somevalue", 
        "DataType": "string" 
       },  
       {"AttributeId": "ns:c3isp:search-string", 
        "Value": "<DPOS SEARCH STRING>", 
        "DataType": "string" 
       }
     ]}
} 
```

### 0.1.3

implementation connected to DSA Adapter Frontent (just methods CRD)


### 0.1.2

fixed return type of CreateDPO

### 0.1.1

Fixes to REST API

### 0.1.0

Stub implementation
